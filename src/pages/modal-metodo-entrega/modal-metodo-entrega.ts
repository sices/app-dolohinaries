import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { CarritoService } from '../../services/carrito-service';
import { GaleriaService } from '../../services/galeria-service';
import { LogService } from '../../services/logs-service'


/**
 * Generated class for the ModalFotoFullPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-metodo-entrega',
  templateUrl: 'modal-metodo-entrega.html',
})
export class ModalMetodoEntregaPage {

  public itemCarrito : any
  public seleccion : any
  public testigo: any //nos dices si hay seleccion en galeria
  metodosEntregaStates = [
   /* {
      icon: "assets/imgs/icons/usb-stroke.png",
      relleno: "assets/imgs/icons/usb-relleno.png",
      selected: false,
      method: 'usb',
      id: 3
    },*/
    {
      icon: "assets/imgs/icons/mail-stroke.png",
      relleno:"assets/imgs/icons/mail-relleno.png",
      selected: false,
      method: 'mail',
      id: 1
    },
    {
      icon:"assets/imgs/icons/impresora-stroke.png",
      relleno:"assets/imgs/icons/impresora-relleno.png",
      selected: false,
      method: 'impresora',
      id:2
    },
    {
      icon:"assets/imgs/icons/cd-stroke.png",
      relleno:"assets/imgs/icons/cd-relleno.png",
      selected: false,
      method: 'cd',
      id: 4
    }


  ];

  methods = ['usb','mail','impresora','cd']
  galeria: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public carrito: CarritoService, public viewCtrl: ViewController, public logservice: LogService) {
    //this.itemCarrito = navParams.get('itemCarrito')
    this.galeria = navParams.get('galeria');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalMetodoEntregaPage');
    this.logservice.accion.push(" ++ en modal metodo entraga ");
    
    
  }

  agregarAMetodo(index){
      //let currentIndex = this.slides.getActiveIndex();
      console.log("indeeex  ");
      //console.log(index);
      //console.log("indexxx metodo ");
      //console.log(index.method);
      var numero = this.galeria.miniaturasTest.length;
      this.carrito.agregarMultiple(this.galeria.selectMultiple, index, numero);
      index.selected = true;
      this.galeria.seleccion = true;
      this.galeria.miniaturasTest.forEach(element => {
        element.selected = true;
        this.carrito.fotosMultiple.push(element);
      });
      //this.galeria.metodoMultiple.push(index)
      //this.carrito.agregar( this.galeria.miniaturasTest[currentIndex], itemMetodo )
      this.logservice.accion.push(" agregarAMetodo abrir modal numero: "+numero+" this.galeria.selectMultiple: "+this.galeria.selectMultiple+" index: "+index.method+" this.galeria.miniaturasTest.length "+this.galeria.miniaturasTest.length+"  ");
    
    }

    okei(){
      this.viewCtrl.dismiss();
      this.logservice.accion.push(" click en modal entrega OK ");
    
    }
    
    finalizarSeleccion(){
      this.logservice.accion.push(" click en modal finalizarSeleccion MODAL ");
      this.galeria.seleccion = false;
      this.viewCtrl.dismiss();
    }

}
