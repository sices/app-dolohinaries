import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FotobruApiService } from '../../services/fotobru-api-service';

/**
 * Generated class for the ConfiguracionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-configuraciones',
  templateUrl: 'configuraciones.html',
})
export class ConfiguracionesPage {

  public data = {
    ip : '10.10.10.141'
  }

  constructor(public navCtrl: NavController, public navParams: NavParams,  public viewCtrl: ViewController, public api: FotobruApiService ) {
      this.data.ip = this.api.localIp;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfiguracionesPage');
  }

  actionCancelar(){
    this.viewCtrl.dismiss(null);
  }
  actionCambiar(){


    this.viewCtrl.dismiss(this.data.ip);
  }

}
