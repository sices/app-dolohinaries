import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { FotobruApiService } from './fotobru-api-service';
import {Observable} from 'rxjs/Observable'
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/catch';
import { AlmacenamientoService } from './almacenamiento-service';

import Config from './config';
declare var Fotobru;

@Injectable()
export class GaleriaService{


    /**
     * Array de la siguiente forma
     *
     */
    public galeriaFotos = [

    ];

    public today = new Date();
    //split
    public dd = this.today.getDate();
    public mm = this.today.getMonth() + 1; //because January is 0! 
    public yyyy = this.today.getFullYear();

    public currentDate = this.yyyy+"-"+this.mm+"-"+this.dd;


    public miniaturasTest = [];//miniaturatest original
    public miniaturasPre = [];

    public totalImgs = null;
    public prese : any;  //bandera que nos dice si estamos en seleccion
    public zona : any;
    public seleccion : any;  //bandera que nos dice si estamos en seleccion
    public selectMultiple: any; // guardaremos el item para el carrito
    public presecarray = [];
    public miniaturasAux = [];
    public filtro:any;



    /**
     *
     */
    constructor(public api: FotobruApiService, public almacenamiento: AlmacenamientoService) {
         this.inicializarArrayMinaturas()
    }

    settearDatosPrueba(){
        let _ctx = this

        let array = [
            'https://bloximages.newyork1.vip.townnews.com/stltoday.com/content/tncms/assets/v3/editorial/9/2d/92d83079-6ac4-5541-9714-b54cf9056043/58f8fcbdba8cc.image.jpg?resize=1200%2C796',
            'https://img.chilango.com/2017/11/Six-Flags.jpg',
            'https://img.grouponcdn.com/deal/3BotWzr1uzBAYCochggupiNvfHjz/3B-700x420/v1/c700x420.jpg',
            'https://cache.undercovertourist.com/media_file/six-flags-magic-mountain-6467.jpg',


            'https://bloximages.newyork1.vip.townnews.com/stltoday.com/content/tncms/assets/v3/editorial/9/2d/92d83079-6ac4-5541-9714-b54cf9056043/58f8fcbdba8cc.image.jpg?resize=1200%2C796',
            'https://img.chilango.com/2017/11/Six-Flags.jpg',
            'https://img.grouponcdn.com/deal/3BotWzr1uzBAYCochggupiNvfHjz/3B-700x420/v1/c700x420.jpg',
            'https://cache.undercovertourist.com/media_file/six-flags-magic-mountain-6467.jpg',

            'https://bloximages.newyork1.vip.townnews.com/stltoday.com/content/tncms/assets/v3/editorial/9/2d/92d83079-6ac4-5541-9714-b54cf9056043/58f8fcbdba8cc.image.jpg?resize=1200%2C796',
            'https://img.chilango.com/2017/11/Six-Flags.jpg',
            'https://img.grouponcdn.com/deal/3BotWzr1uzBAYCochggupiNvfHjz/3B-700x420/v1/c700x420.jpg',
            'https://cache.undercovertourist.com/media_file/six-flags-magic-mountain-6467.jpg',


            'https://bloximages.newyork1.vip.townnews.com/stltoday.com/content/tncms/assets/v3/editorial/9/2d/92d83079-6ac4-5541-9714-b54cf9056043/58f8fcbdba8cc.image.jpg?resize=1200%2C796',
            'https://img.chilango.com/2017/11/Six-Flags.jpg',
            'https://img.grouponcdn.com/deal/3BotWzr1uzBAYCochggupiNvfHjz/3B-700x420/v1/c700x420.jpg',
            'https://cache.undercovertourist.com/media_file/six-flags-magic-mountain-6467.jpg'
        ];

        var counter = 0;
        //http://www.pngmart.com/files/1/Video-Icon-PNG-Free-Download-279x279.png

      /*  _ctx.miniaturasTest.push({
            miniatura : "http://www.pngmart.com/files/1/Video-Icon-PNG-Free-Download-279x279.png",
            slider : "http://www.pngmart.com/files/1/Video-Icon-PNG-Free-Download-279x279.png",
            idfoto: 1000022,
            idzona: 1,
            selected: false
        })*/

        /*_ctx.miniaturasTest.push({
            miniatura : "https://cdn0.iconfinder.com/data/icons/line-action-bar/48/select-512.png",
            slider : "https://cdn0.iconfinder.com/data/icons/line-action-bar/48/select-512.png",
            idfoto: 1123987,
            idzona: 1,
            selected: false
        })*/

        array.forEach( element => {
            _ctx.miniaturasTest.push({
                miniatura : element,
                slider : element,
                idfoto: 1 * 100 +  counter,
                idzona: 1
            })
            counter += 1;
        });

    }
    //
    inicializarArrayMinaturas(){
        let _ctx = this

        if(_ctx.filtro == true){
            console.log("modo filtro ON");
        }

        if( Config.ModoMockData == true){

            this.settearDatosPrueba()

            return;
        }else if (this.prese == true) {
            this.modepreselec();

            return;
        }else{


        _ctx.api.obtenerApiKey((apikey) => {
            _ctx.filtro = true;
            if(_ctx.filtro == true){
                console.log("modo filtro ON");
                _ctx.api.buildPeticion(
                    'sales/fotoszonafiltro', //modf filtro                                                                //fecha la obtenemos de alguna variable
                    "ApiKey=" + apikey+'&idZona=' + _ctx.zona.idzona+'&idHorario=' + _ctx.zona.idhorario + '&fecha=' + _ctx.currentDate,  //aqui sera para filtro
                  ).subscribe( (data: any) => {
                      console.log("Obteniendo las imagenes que en local deberia haber guardado y confirmado")
                    console.log(data)
                      data.forEach(element => {
                        if(element.idzona == _ctx.zona.idzona)

                                _ctx.miniaturasTest.push({
                                        miniatura :  element.ruta,
                                        slider : element.ruta,
                                        idfoto: element.idfoto,
                                        idzona: element.idzona
                                })

                      })

                  })

            }else{
          _ctx.api.buildPeticion(
            'sales/fotoszona',
            "ApiKey=" + apikey+'&idZona=' + _ctx.zona.idzona,
          ).subscribe( (data: any) => {
              console.log("ELSE Obteniendo las imagenes que en local deberia haber guardado y confirmado")
            console.log(data)
              data.forEach(element => {
                if(element.idzona == _ctx.zona.idzona)

                        _ctx.miniaturasTest.push({
                                miniatura :  element.ruta,
                                slider : element.ruta,
                                idfoto: element.idfoto,
                                idzona: element.idzona
                        })

              })

          })
         }
        })

    }//else
  }

    /**
     * Metodo llamado por la clase EventosServerService
     * */
    agregarFoto(foto){
        // Si pertenece a la zona, muestra en galeria
        if(foto.idzona == this.zona.idzona)
            this.miniaturasTest.push(foto)
    }

    removeArray(item){
     this.presecarray.forEach((foto, index) => {
       if(foto.idfoto == item.idfoto){
          this.presecarray.splice(index,1)
       }
     });

     console.log("menos uno preselec");
     console.log(this.presecarray);

  }

  modepreselec(){ //verifica si estamos el mode preselec para asignar miniaturas
    console.log("mode preselec");
    let _ctx = this

    if( _ctx.prese == true){
      _ctx.miniaturasAux =  _ctx.miniaturasTest;
      _ctx.miniaturasTest = [];
      _ctx.presecarray.forEach(element => {
         console.log("for eaCH");
        if(element.idzona == _ctx.zona.idzona){
                _ctx.miniaturasTest.push({
                        miniatura :  element.miniatura,
                        slider : element.slider,
                        idfoto: element.idfoto,
                        idzona: element.idzona
                })
            }

      })
    }

  }

  leaveselec(){
    console.log("dejando leaveselec");  
    let _ctx = this
    _ctx.miniaturasTest =  _ctx.miniaturasAux;
    _ctx.miniaturasAux = [];
    this.prese = false;
    this.presecarray.forEach(element => {
        element.selected = false;
    });
    this.presecarray = [];
    
  }




}
