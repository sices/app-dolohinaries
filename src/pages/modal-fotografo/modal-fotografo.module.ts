import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalFotografoPage } from './modal-fotografo';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ModalFotografoPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalFotografoPage),
    TranslateModule.forChild()
  ],
})
export class ModalFotografoPageModule {}
