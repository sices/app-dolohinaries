import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalClientePage } from './modal-cliente';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ModalClientePage,
  ],
  imports: [
    IonicPageModule.forChild(ModalClientePage),
    TranslateModule.forChild()
  ],
})
export class ModalClientePageModule {}
