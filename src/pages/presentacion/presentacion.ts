import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { GaleriaService } from '../../services/galeria-service';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { CarritoService } from '../../services/carrito-service';
import { TranslateService } from '@ngx-translate/core';
import { ModalMetodoEntregaPage } from '../modal-metodo-entrega/modal-metodo-entrega';
import Config from '../../services/config';
import { LogService } from '../../services/logs-service'


/**
 * Generated class for the PresentacionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-presentacion',
  templateUrl: 'presentacion.html',
})
export class PresentacionPage {

  @ViewChild(Slides) slides: Slides;

  miniaturas :  any;
  FotoSlides: any;

  metodosEntregaStates = [
    //{
    //  icon: "assets/imgs/icons/usb-stroke.png",
    //  relleno: "assets/imgs/icons/usb-relleno.png",
    //  selected: false,
    //  method: 'usb',
    //  id: 3
    //},
    {
      icon: "assets/imgs/icons/mail-stroke.png",
      relleno:"assets/imgs/icons/mail-relleno.png",
      selected: false,
      method: 'mail',
      id: 1
    },
    {
      icon:"assets/imgs/icons/impresora-stroke.png",
      relleno:"assets/imgs/icons/impresora-relleno.png",
      selected: false,
      method: 'impresora',
      id:2
    },
    {
      icon:"assets/imgs/icons/cd-stroke.png",
      relleno:"assets/imgs/icons/cd-relleno.png",
      selected: false,
      method: 'cd',
      id: 4
    }


  ];
  public galeria: GaleriaService;
  public itemV:any;
  public itemT:any;
  public testigowm = true;

  statesSlides = []
  methods = ['usb','mail','impresora','cd']

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public carrito: CarritoService  , public alertCtrl: AlertController,
    public translate: TranslateService,
    public loadingCtrl: LoadingController,
    public logservice: LogService,
    public modalCtrl: ModalController)
  {

    let _ctx = this
    this.miniaturas = [];
    this.FotoSlides= [];

    this.galeria = this.navParams.get('galeria')
    let inputIndex = this.navParams.get('index')

    //TODO: mandar por parametro la foto seleccionada en la galeria
    this.actualizarStates( this.galeria.miniaturasTest[inputIndex] )

  }
  ionViewWillLeave(){
    this.logservice.accion.push(" >>> ionViewWillLeave this.galeria.prese: "+this.galeria.prese+" ");
    if(this.galeria.prese == true){
       this.galeria.leaveselec();
    }
    this.carrito.pagePresentacion = null
  }

  loading = null

  mostrarCargando(){
    let _ctx = this

    this.translate.get('CART_WAIT_1').subscribe((res: string) => {

        console.log(res);
        //=> 'hello world'
        _ctx.loading = this.loadingCtrl.create({
          content: res
        });

        _ctx.loading.present();

    });

  }

  quitarCargando(){
    let currentIndex = this.slides.getActiveIndex();
    let _ctx = this

    var quitarLoading = function(){
      if(_ctx.loading != null)
        _ctx.loading.dismiss();
      _ctx.actualizarStates(_ctx.galeria.miniaturasTest[currentIndex])
    }

    if(Config.ModoMockData)
      setTimeout(() => {
        //prueba, simulando que de verdad se estan recalculando los precios
        quitarLoading()
      }, 1);
    else
      quitarLoading()

  }

  compraRealizada(value){
    this.logservice.accion.push("/> COMPRA REALIZADA </ ");
    this.settearIconosMetodosEntrega(false)
  }
  carritoVaciado(value){
    this.logservice.accion.push("/> CARRITO VACIADO </ ");
    this.settearIconosMetodosEntrega(false)
  }


  settearIconosMetodosEntrega(bandera){
    for(var i = 0; i < this.metodosEntregaStates.length; i++){
      this.metodosEntregaStates[i].selected = bandera;
    }
  }

  scrollearPresentacion(index){
    this.slides.slideTo(index)
  }
  actualizarStates(item){

    this.settearIconosMetodosEntrega(false)

    let agregadas = this.carrito.buscarAgregadasPorId(item.idfoto)
    agregadas.forEach(value=>{

      for(var i = 0; i < this.metodosEntregaStates.length; i++){
        if(this.metodosEntregaStates[i].id == value.idtipoentrega)
          this.metodosEntregaStates[i].selected  = true
      }

    })

  }

  limpiarMetodo(fotoid, idtipoentrega){
    this.logservice.accion.push(" limpiarMetodo fotoid: "+fotoid+" idtipoentrega "+idtipoentrega+"");
    let currentIndex = this.slides.getActiveIndex();
    let currentElement = this.galeria.miniaturasTest[currentIndex]
    if(fotoid != currentElement.fotoid)
      return

    for(var i = 0; i < this.metodosEntregaStates.length; i++){
      if(this.metodosEntregaStates[i].id == idtipoentrega)
        this.metodosEntregaStates[i].selected = false;
    }
  }

  // llamado por la vista
  agregarAMetodo(itemMetodo){
    // if(itemMetodo.id == 4)
    //   return
    this.logservice.accion.push(" agregarAMetodo itemMetodo: "+itemMetodo+" ");
    let currentIndex = this.slides.getActiveIndex();
    console.log("current index "+currentIndex);
    // if(!itemMetodo.selected)
    //itemMetodo.selected = true;
      //currentIndex = currentIndex + 2 //le vamos a sumar dos por el boton de video y de galeria
      this.carrito.agregar( this.galeria.miniaturasTest[currentIndex], itemMetodo )
    // else
    //   this.carrito.removerCarritoMetodo( this.galeria.miniaturasTest[currentIndex], itemMetodo )

    // itemMetodo.selected = !itemMetodo.selected
  }

  slideChanged(){
    // let elementoItemCarrito = buscarEnCarrito();
    // actualizar elementos
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', currentIndex);
    var element = this.galeria.miniaturasTest[currentIndex]
    console.log('fotoid = ' + element.idfoto)
    this.actualizarStates(element)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PresentacionPage');
    this.carrito.pagePresentacion = this
    Config.ModoMockData = false;
    if(Config.ModoMockData){
      this.mockAgregarAlCarrito()
    }else if(this.galeria.prese == true){
        this.galeria.modepreselec();
      }
    }




  mockAgregarAlCarrito(){
      let  _ctx = this
      let tiempo = 100;

    setTimeout( () => {
        setTimeout( () => _ctx.carrito.agregar( _ctx.galeria.miniaturasTest[0], _ctx.metodosEntregaStates[0]) , tiempo )
        setTimeout( () => _ctx.carrito.agregar( _ctx.galeria.miniaturasTest[0], _ctx.metodosEntregaStates[1]) , tiempo + 100 )
        setTimeout( () => _ctx.carrito.agregar( _ctx.galeria.miniaturasTest[0], _ctx.metodosEntregaStates[2]) , tiempo + 200 )
        setTimeout( () => _ctx.carrito.agregar( _ctx.galeria.miniaturasTest[1], _ctx.metodosEntregaStates[0]) , tiempo + 300)
        setTimeout( () => _ctx.carrito.agregar( _ctx.galeria.miniaturasTest[1], _ctx.metodosEntregaStates[1]) , tiempo + 400 )
        setTimeout( () => _ctx.carrito.agregar( _ctx.galeria.miniaturasTest[1], _ctx.metodosEntregaStates[2]) , tiempo + 500)
    },1000)

  }

  ionViewDidEnter(){
    let index = this.navParams.get('index')
    if(index != null){
      this.scrollearPresentacion(index)
    }

  }

  comprarVideo(){
    console.log("comprar video")
    this.itemV = { "miniatura" : "assets/imgs/Video-Icon-PNG-Free-Download.png",
    "slider" : "assets/imgs/Video-Icon-PNG-Free-Download.png",
    "idfoto": 1000022,
    "idzona": 1,
    "selected": false }
    this.carrito.agregarMultiple(this.itemV, 4, 1);
    this.logservice.accion.push(" comprarVideo dentro de presentacion ");
  }

  comprarTodo(){
    console.log("seleccion all fotos");
    this.itemT = {
        "miniatura" : "assets/imgs/select-512.png",
        "slider" : "assets/imgs/select-512.png",
        "idfoto": 1123987,
        "idzona": 1,
        "selected": false
    }

    this.galeria.selectMultiple = this.itemT;
    this.logservice.accion.push(" comprarTodo abrir modal ");
    this.verModal();
    return ;
  }

  verModal(){
    console.log("ver modal para metodo de entrega de todas las fotos");
    let modal = this.modalCtrl.create( ModalMetodoEntregaPage, { "galeria" : this.galeria })
    this.logservice.accion.push(" verModal abrir modal ");
    
    modal.present()
  }
  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'Codigo Incorrecto',
      subTitle: 'El codigo que introdujo no es correcto.',
      buttons: ['OK']
    });
    alert.present();
  }

  showMarcaAgua() {
    const prompt = this.alertCtrl.create({
      title: 'Marca de Agua',
      message: "Ingrese la contraseña para quitar la marca de agua",
      inputs: [
        {
          name: 'contrasenia',
          placeholder: 'Contrañea',
          type:'password'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Aceptar',
          handler: data => {
            console.log('Saved clicked');
            console.log(data);
            if(data.contrasenia == "21570r"){
              this.testigowm = false;
            }else{
              this.showAlert();
            }
          }
        }
      ]
    });
    prompt.present();
  }

  quitarwm(){
    console.log("Quitar marca de agua");
    if(this.testigowm){
      this.showMarcaAgua();
    }
    //this.testigowm = false;

  }

  iionViewWillLeave(){
    this.testigowm = true;
  }


}
