import { Injectable } from '@angular/core';
import { Http, Headers, RequestMethod, URLSearchParams , RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/catch';

declare var Fotobru : any;

@Injectable()
export class FotobruApiService {
    public luis = 'http://10.10.10.120';
    public marco =  'http://10.10.10.129';
    public prod = 'http://10.10.1.1';
    public prodTulum = "http://10.10.40.112"
    public prodCozuTulum = "http://10.10.40.113";
    public prodRiviera = "http://10.10.30.111"
    public casitamarilla = "http://192.168.0.54";
    public cozumel = "http://10.10.201.66";
    public casablanca = "http://192.168.1.163";
    public localhost = "http://127.0.0.1";
    public barcelo = "http://10.10.50.110";
    public casa = "http://192.168.1.70";
    public macoficina = "http://10.10.10.125";
    

    public target = this.macoficina;

    public baseUrl = this.target+'/fotobru-api/web/index.php?' ;
    public baseIp = this.target  //10.10.10.120
    public cachedApiKey = '';
    public localIp = '';
    /**
     *
     */
    constructor(public http: Http) {

      if( (typeof(Fotobru)) !== 'undefined' ){
        var configuraciones = Fotobru.leerConfiguraciones()
        if(configuraciones){
          this.cambiarIp( configuraciones.ApiUrl )
          this.localIp = configuraciones.IpCliente
          console.log("leyendo las cnfiguraciones desde un json")
        }else{
          console.log("error al leer json configs")
          this.cambiarIp(this.target+'/fotobru-api/web/index.php?r=')
        }
      }
      else{
        console.log("foto bru no esta definido en apiservice")
        // mock up server
        this.cambiarIp(this.target+'/fotobru-api/web/index.php?r=')
      }

    }

    public cambiarIp(ip){
      // this.baseUrl = ip + '/fotobru-api/web/index.php?r='
      // this.baseIp = ip

      //mock up server y dolphinaris
      this.baseIp =  this.baseUrl = ip

    }

    public buildPeticion(route , bodyParameters){
      //TODO: new puede ser que este gastando memoria
      let headers = new Headers();
      headers.append("Content-Type", "application/x-www-form-urlencoded");

      return this.http.request(
        // this.baseUrl + 'r=' + route,
        this.baseUrl + route,
        {
          body: bodyParameters,
          method: RequestMethod.Post,
          headers: headers,
          withCredentials: false
        }
      )
        .map(response => response.json())
        .map(data => {
            return data;
        });
    }

    obtenerApiKey(fnApiKey){

      let headers = new Headers();
      headers.append("Content-Type", "application/x-www-form-urlencoded");

      let _ctx = this
        this.buildPeticion(
        'sesion/apikeyestacion',
        'ip=192.1.1.1&password=123456'    //'ip='+_ctx.localIp+'&password=123456'
        )
        .subscribe(
          (data) => {
            if(data.Code == 0){
                console.log('Api key obtenido')
                console.log(data.ApiKey)
                _ctx.cachedApiKey = data.ApiKey
                fnApiKey(data.ApiKey)
            }
          },
          (error) => {
            console.log("Error al obtener apikey")
            console.error(error);
          }
        )
    }

    checkServer(){

      /*let route = '/fotobru-api/web/index.php?r=api/ping'
      let _ctx = this

      this.http.get(this.target+route).timeout(2000).subscribe(
        data => console.log(data),
        error => {
                   console.log("erro ocurrio", error)
                   if (this.target == this.luis) {
                       this.target = this.marco
                   }else{
                    this.target = this.luis
                   }
                  }
       );

      /*console.log("checserver func")
      let _ctx = this
      let route = '/fotobru-api/web/index.php?r=api/ping'
      let headers = new Headers();
      headers.append("Content-Type", "application/x-www-form-urlencoded");

       this.http.request(
        // this.baseUrl + 'r=' + route,
        this.prod + route,
        {
          method: RequestMethod.Post,
          headers: headers,
          withCredentials: false
        }
      )
        .map(response => response.json())
        .map(data => {
            console.log(data);
            return data;
        },err => {
          console.log("Error occured. err")
        });


       */



        /*this.buildPeticion(
        'api/ping',
        ''
        )
        .timeout(5000)
        .subscribe(
          (data) => {
            if(data.Code == 0){
                console.log('servidor arriba')

            }
          },
          (error) => {
            if(this.prod == this.luis){
              this.prod = this.marco
            }else{
              this.prod = this.luis
            }
            console.log("Servidor no responde -- servidor cambiado" + this.prod)
            console.error(error);
          }
        )*/

    }
}
