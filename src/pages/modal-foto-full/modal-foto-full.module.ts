import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalFotoFullPage } from './modal-foto-full';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ModalFotoFullPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalFotoFullPage),
    TranslateModule.forChild()
  ],
})
export class ModalFotoFullPageModule {}
