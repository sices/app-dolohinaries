import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams , AlertController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the ModalCorreoEntregaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-correo-entrega',
  templateUrl: 'modal-correo-entrega.html',
})
export class ModalCorreoEntregaPage {

  public data = {
    correo : '',
    correo2 : ''
  }



  listadecorreos = [

  ];




  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,  public alertCtrl: AlertController, public translate: TranslateService) {
      let correo = this.navParams.get('correo')
    if(correo != null ){
      this.data.correo = correo
      this.data.correo2 = correo

    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalCorreoEntregaPage');
    this.listadecorreos.push({
      correo : '',
      correo2 : '',
    })

  }

  comparaCorreos(correosArray){
    console.log("dentro de correos array");
    let test = correosArray[0].correo.includes("@");
    if(!test){
      console.log("SI LO INCLUYE");
      return false;
    }
    for(var i=0 ; i < correosArray.length; i++){
          console.log(correosArray[i]);
          console.log(correosArray[i].correo);
          console.log(correosArray[i].correo2);
        if(correosArray[i].correo != correosArray[i].correo2 ){
          console.log("no son iguales");
          return false;
        }
    }
    console.log("Son iguales");
   return true;
  }

  actionModalCorreoEntregaOk(){
    console.log(this.listadecorreos);
    var iguales = this.comparaCorreos(this.listadecorreos);
    /* DEVOLVER LOS DATOS AQUI*/
    /*var iguales = (this.data.correo == this.data.correo2)
    */
    console.log(iguales);
    if(!iguales){

      this.translate.get('NOMATCH_1').subscribe((title: string) => {
      this.translate.get('NOMATCH_2').subscribe((subtitle: string) => {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: subtitle,
          buttons: ['Ok']
        });
        alert.present();
      });
      });
      return;
    }

    //if(this.data.correo.length == 0 && this.data.correo.length == 0)
    //  return;

    this.viewCtrl.dismiss(this.listadecorreos);//this.data
  }


  actionModalCorreoEntregaCancelar(){

    this.viewCtrl.dismiss(null);
  }

  actionModalCorreoAgregarCampo(){
    this.listadecorreos.push({
      correo : '',
      correo2 : '',
    })
  }
}
