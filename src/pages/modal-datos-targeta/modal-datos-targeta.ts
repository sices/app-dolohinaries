import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ModalDatosTargetaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-datos-targeta',
  templateUrl: 'modal-datos-targeta.html',
})
export class ModalDatosTargetaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalDatosTargetaPage');
  }
  actionModalPagar(){
    //TODO: DEVOLVER LOS DATOS AQUI
    this.viewCtrl.dismiss();
  }

  actionModalCancelar(){
    this.viewCtrl.dismiss();
  }
}
