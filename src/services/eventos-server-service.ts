import { Injectable } from '@angular/core';
import { Http, Headers, RequestMethod, URLSearchParams , RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { FotobruApiService } from './fotobru-api-service';
import { AlmacenamientoService } from './almacenamiento-service';
import { GaleriaService } from './galeria-service';


/**
 * NO es singleton
 */
@Injectable()
export class EventosServerService{


    public zona : any
    public galeria: GaleriaService
    public totalImgs : number
    public continuar = true 
    /**
     *
     */
    constructor(public api: FotobruApiService, 
        public almacenamiento: AlmacenamientoService,
    ) {

    }

    // TODO: POR USAR
    pedirCualesDeboMostrar(){
        return new Promise((resolve,reject)=>{
            resolve([])
        })
    }

    ngOnDestroy() {
        console.log('EVS destroy!')
        this.continuar = false
        // this.galeria = null
    }

    empezarAPedirFotos(galeria: GaleriaService, zona: any){
        // leer si hay imagenes en storage segun el almacenamiento
        this.zona = zona
        this.galeria = galeria
        // this.doSomething(zona)
    }

    // DE galeria-service
    private doSomething(zona){
        if(this.zona != null)
            this.zona = zona
        this.api.obtenerApiKey(this.respuesta.bind(this,zona))
    }

    descargarImagenPendiente(apikey,zona){
        if(!this.continuar)
            return
        let _ctx = this
        let prefix = "data:image/jpg;base64,";


        _ctx.observableCopiarImagen(apikey,zona)

        .subscribe( (response) => {
            /* la respuesta del server tiene la siguiente forma
                {
                    Code: "0",
                    Mesage: "Operacion Completa"
                    FotoSlider: "fullurl"
                    Miniatura: "fullurl"
                    fechacarga: "2018-05-22"
                    idfoto: 31,
                    idzona: 1
                    nombre: "archivo.jpg"
                }
            */
            console.log('Despues de consular el endpoint sales/copiarimagen...')
            // console.log(response)

            var imgResponse = {
                miniatura: response.Miniatura,
                slider: response.FotoSlider,
                idfoto: response.idfoto,
                idzona: response.idzona, 
                nombre: response.nombre
            }

            // galeria
            if(_ctx.continuar)
                _ctx.galeria.agregarFoto(imgResponse)

             _ctx.observableConfirmarImagen(apikey, response.idfoto)
                .subscribe( (response2) => {
                    console.log("Imagen confirmada, continuar = "  + _ctx.continuar)
                    _ctx.respuesta(zona, apikey)

                })

     
        },(e)=>{
            console.log("finalizando de descargar")
        })


    }
    observableCopiarImagen(apikey,zona){
        return this.api.buildPeticion(
            'sales/copiarimagen',
            "ApiKey=" + apikey+'&idZona='+zona.idzona,
        );
    }
    observableConfirmarImagen(apikey, idfoto){
        return this.api.buildPeticion(
                'sales/confirmarimagen',
                "ApiKey=" + apikey+'&ImgId='+idfoto,
        );
    }

    respuesta(zona, apikey){
        if(!this.continuar)
            return

        console.log('Galeria service, respuesta method')
        console.log(apikey)
        // console.log(zona)
        let _ctx = this
        let prefix = "data:image/jpg;base64,";

        if(this.totalImgs == null){
            this.api.buildPeticion(
                'sales/numeroimagenes',
                "ApiKey=" + apikey+'&idZona='+zona.idzona,
            ).subscribe( (data) => {
                //data es un numero, el total de imagenes disponibles que puedo leer de cualquier zona, dependiendo del log del webservice
                console.log('despues de consular el endpoint sales/numeroimagenes...')
                console.log(data)
                _ctx.totalImgs = data


                if(data > 0){
                    // for (let index = 0; index < data; index++) {
                        _ctx.descargarImagenPendiente( apikey, zona  )

                    // }
                }
            })
        }else{
            if(this.totalImgs > 0)
                _ctx.descargarImagenPendiente( apikey, zona  )
        }

    }
}