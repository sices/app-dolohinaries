import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GaleriaPage } from './galeria';
import { ComponentsModule } from '../../components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    GaleriaPage,
  ],
  imports: [
    IonicPageModule.forChild(GaleriaPage),
		TranslateModule.forChild({
			isolate: false
		}),
    ComponentsModule
  ],

})
export class GaleriaPageModule {}
