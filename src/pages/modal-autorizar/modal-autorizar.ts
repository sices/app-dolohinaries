import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { CarritoService } from '../../services/carrito-service';
import { GaleriaService } from '../../services/galeria-service';

/**
 * Generated class for the ModalFotoFullPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-autorizar',
  templateUrl: 'modal-autorizar.html',
})
export class ModalAutorizar {

  public itemCarrito : any
  public seleccion : any
  public testigo: any //nos dices si hay seleccion en galeria
  public precio = ""
  public codigo = ""
  private victorCode = "21570r"
  private xiomaraCode = "3108475a"
  galeria: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public carrito: CarritoService, public viewCtrl: ViewController, public alertCtrl: AlertController) {
    //this.itemCarrito = navParams.get('itemCarrito')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalMetodoEntregaPage');

  }

  agregarAMetodo(index){
      //let currentIndex = this.slides.getActiveIndex();
      console.log("indeeex  ");
      //console.log(index);
      //console.log("indexxx metodo ");
      //console.log(index.method);
      var numero = this.galeria.miniaturasTest.length;
      this.carrito.agregarMultiple(this.galeria.selectMultiple, index, numero);
      index.selected = true;
      this.galeria.seleccion = true;
      this.galeria.miniaturasTest.forEach(element => {
        element.selected = true;
        this.carrito.fotosMultiple.push(element);
      });
      //this.galeria.metodoMultiple.push(index)
      //this.carrito.agregar( this.galeria.miniaturasTest[currentIndex], itemMetodo )
    }

    okei(){
      console.log("Nombre");
      console.log(this.precio);
      console.log("Codigo");
      console.log(this.codigo);

       if(this.precio.length > 1){
         if(this.codigo.length == 0){
          let alert = this.alertCtrl.create({
            title: "Codigo nulo",
            message: "No ha introducido un codigo",
            cssClass : 'alert-compra',
            buttons: ['Ok']
          });
          alert.present();
          return ;
         }
      } 

      
      if (this.codigo == "3108475a") {
        console.log("xiamara");
        if(parseInt(this.precio) > 1){
          this.viewCtrl.dismiss(this.precio);
        }
        
      }else if(this.codigo == "21570r"){
        console.log("victor");
        if(parseInt(this.precio) > 1){
          this.viewCtrl.dismiss(this.precio);
        }
        
      }else if(this.codigo == "0000"){
        console.log("vendedor")
           
            this.precio = "0";
            this.viewCtrl.dismiss(this.precio);
           
      }else{
        if(this.codigo.length > 3){

          
          let alert = this.alertCtrl.create({
            title: "Codigo Incorrecto",
            message: "Ha introducido un corrido incorrecto",
            cssClass : 'alert-compra',
            buttons: ['Ok']
          });
          alert.present();
          return ;
  
       
    }
        this.precio = "0";
        this.viewCtrl.dismiss(this.precio);
       /* let alert = this.alertCtrl.create({
          title: "ERROR CODE",
          subTitle: "Codigo no valido",
          buttons: ['Ok']
        });
        alert.present();
      }*/
    }
  }

  

}
