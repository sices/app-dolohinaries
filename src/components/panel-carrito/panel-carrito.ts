
import { Component, EventEmitter, Output } from '@angular/core';
import { ModalController, AlertController, SelectPopover } from 'ionic-angular';
import { ModalDatosTargetaPage } from '../../pages/modal-datos-targeta/modal-datos-targeta';
import { ModalCorreoEntregaPage } from '../../pages/modal-correo-entrega/modal-correo-entrega';
import { CarritoService } from '../../services/carrito-service';

import { Http, Headers, RequestMethod, URLSearchParams , RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { FotobruApiService } from '../../services/fotobru-api-service';
import Config from '../../services/config';
import { ModalFotoFullPage } from '../../pages/modal-foto-full/modal-foto-full';
import { TranslateService } from '@ngx-translate/core';

import { ModalClientePage } from '../../pages/modal-cliente/modal-cliente';
import { ModalDuplicarPage } from '../../pages/modal-duplicar/modal-duplicar';
import { ModalAutorizar } from '../../pages/modal-autorizar/modal-autorizar';
import { LogService } from '../../services/logs-service'


// import { Content } from 'ionic/angular';

/**
 * Generated class for the PanelCarritoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 *
 * 4 tipos de metodo de entrega:
 *  USB
 *  CORREO
 *  IMPRESORA
 *  CD
 */
@Component({
  selector: 'panel-carrito',
  templateUrl: 'panel-carrito.html'
})
export class PanelCarritoComponent {

  public fotosSeleccionadas: any = [];
  public totalAPagar: number = 0;
  public respuesta:any; //respuesta que nos dara el eliminar carrito del carrito service
  public correoCon: any = [];
  public costo: any
  public total: any
  public fotosCarritoSele: any = [];
  public testigoseleccion: any;
  public fotosOriginal:any = [];
  constructor(public modalCtrl: ModalController, public alertCtrl: AlertController,public carrito: CarritoService
    , private translate: TranslateService
    ,public http: Http
    ,public api: FotobruApiService
    ,public logservice: LogService
  ) {
    console.log('Hello PanelCarritoComponent Component');







    this.fotosSeleccionadas = [
      { imagen : 'cuponeate.png', metodoImg: 'cd-carrito.png', precio: 99.0 },
      { imagen : 'node.png', metodoImg: 'usb-carrito.png', precio: 100.0 },
      { imagen : 'globaloxs.png', metodoImg: 'impresora-carrito.png', precio: 1.59 },
      { imagen : 'logo.png', metodoImg: 'mail-carrito.png', precio: 99.9 }
    ]
    this.totalAPagar = 130.00;
  }

   private cachedCorreo = "";

  actionPagar(){
    console.log("pagando...")
    //checar si el carrito tiene uno con metodo de entrega correo
    this.logservice.accion.push(" --> PANEL CARRITO actionPagar ");
    var pedirCorreo = this.carrito.hayImagenConEntregaCorreo()
    let _ctx = this
    _ctx.costo = _ctx.carrito.total;
    if(pedirCorreo){
      this.logservice.accion.push(" -- PANEL CARRITO hay imagen con correo modal correo ");
    
      let modal = this.modalCtrl.create(ModalCorreoEntregaPage, { "correo" : this.cachedCorreo })
      let modalCliente = this.modalCtrl.create(ModalClientePage)
      let modalAutoriza = this.modalCtrl.create(ModalAutorizar)
      modal
      .onDidDismiss(data => {
        if(data){
          var correos = "";
          for(var i = 0; i < data.length ; i++){
            this.logservice.accion.push(" -- DESPUES DE CORREO CORREO: "+data[i].correo+" -- ");
            if(data.length == 1){
              var corr = data[i].correo;
              correos = corr;
            }else{
              var corr = data[i].correo;
              var masu = i + 1;
              if(masu == data.length){
                correos += corr;
              }else{
                correos += corr+",";
              }
            }
          }
          this.cachedCorreo = data == null ? '' : correos;
          console.log("Datos V");
          console.log(data);
          modalAutoriza.present()
          modalAutoriza.onDidDismiss(data => {
           if (data != null) {
            _ctx.total = data;
            modalCliente.present()
            modalCliente.onDidDismiss( formulario => {
              if(formulario != null)
                _ctx.afterDismiss(data, formulario)
            });
          }
          })


        }
        else
          return
      });
      modal.present()
    }else{
          let modalCliente = this.modalCtrl.create(ModalClientePage)
          let modalAutoriza = this.modalCtrl.create(ModalAutorizar)
          modalAutoriza.present()
          modalAutoriza.onDidDismiss(data => {
          if(data != null){
          _ctx.total = data
          modalCliente.present()
          modalCliente.onDidDismiss( formulario => {
            if(formulario != null)
              _ctx.afterDismiss(null, formulario)
          });
        }

    });

  }

}

  @Output() compraRealizada: EventEmitter<any> = new EventEmitter();
  @Output() carritoVaciado: EventEmitter<any> = new EventEmitter();
  @Output() quitarTodas: EventEmitter<any> = new EventEmitter();


  notificarCompraRealizada(code){
    
    let _ctx = this
    this.logservice.accion.push(" -- PANEL CARRITO notificarCompraRealizada : "+code+" -- ");
    
    this.guardarlog(code);
    this.translate.get('CART_ALERT_1_TITLE').subscribe((title: string) => {
      this.translate.get('CART_ALERT_1_SUBTITLE').subscribe((subtitle: string) => {

        let sub = subtitle + '<strong>'+ code + '</strong>'
        let alert = this.alertCtrl.create({
          title: title,
          message: sub,
          cssClass : 'alert-compra',
          buttons: ['Ok']
        });
        alert.present();

        _ctx.carrito.vaciar()
        _ctx.compraRealizada.emit(true)

      })
    })

  }


  afterDismiss(data, formCliente){
    // let datosTarjetaModal = this.modalCtrl.create(ModalDatosTargetaPage);
    // datosTarjetaModal.present();
     //this.cachedCorreo = ""

    let _ctx = this

    // TODO: MOVER TODO EL CODIGO DE ESTA FUNCION A OTRA PARTE

    let headers = new Headers();
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    if(data != null){
        console.log("Data V");
        console.log(data);
        //creamos una cadena con los correos separados por comparaCorreos
        //var correo = new String(string);
        //correoCon
        for(var i = 0; i < data.length; i++){
           var cadena = data[i].correo;
           this.correoCon.push(cadena)
           //correo.concat("eewe");
        }
        console.log(this.correoCon);
        console.log(this.correoCon.toString())

        var correos = this.correoCon.toString();
    }

    //if (this.total == 0) {
    //  this.total = this.costo
    //}
    this.logservice.accion.push(" -- PANEL CARRITO lLenando body : "+this.api.cachedApiKey+" -- costo "+this.costo+" total "+this.total+" nombre cliente "+formCliente.nombre+" idvendedor "+formCliente.codigo+" ");
    
    var body = {
        "ApiKey": this.api.cachedApiKey,
        "estatus": "Terminado",
        "correo": data == null ? '' : _ctx.cachedCorreo,
        "fotos": [ ],
        "costo": this.costo,
        "total": this.total,
        "nombreCliente": formCliente.nombre,
        "idVendedor": formCliente.codigo
    }


    if(this.carrito.count == 0){
      this.logservice.accion.push(" -- PANEL CARRITO this.carrito.count : "+this.carrito.count+" ");
    
      this.translate.get('CART_ALERT_2_TITLE').subscribe((title: string) => {
        this.translate.get('CART_ALERT_2_SUBTITLE').subscribe((subtitle: string) => {
          _ctx.presentAlert(title, subtitle)
          return;
        })
      })

    }


    /*if(Config.ModoMockData == true){
        this.notificarCompraRealizada("TEST")
        return;
    }
   */
   this.logservice.accion.push(" -- PANEL CARRITO this.carrito.count : "+this.carrito.count+" ");
    
    this.carrito.items.forEach( element => {
        // fotoid: elemento.idfoto,
        // idtipoentrega: metodo.id,
        if(element){
          console.log("element");
          //for(var i = 0 ;  i < element.count ; i++){
            console.log("prueba -- element.fotoid "+element.fotoid);
            if(element.fotoid == 1123987){ // quiere decir que hay una seleccion multiple de algun metodo
              if(this.carrito.fotosMultiple.length > 0 ){
                var cuenta = element.count;
                for (let index = 0; index < cuenta; index++) {
                   const elemento = this.carrito.fotosMultiple[index];
                   if(elemento){

                      body.fotos.push({
                        idfoto: elemento.idfoto,
                        tipoentrega: element.idtipoentrega
                      })

                  }

                }
                /*this.carrito.fotosMultiple.forEach( elemento => {
                    // fotoid: elemento.idfoto,
                    // idtipoentrega: metodo.id,
                    if(elemento){
                      /*for(var i = 0 ;  i < cuenta ; i++){*/
                      /*  body.fotos.push({
                          idfoto: elemento.idfoto,
                          tipoentrega: element.idtipoentrega
                        })
                      }
                    }
                })*/
              }

            }else{
              if(element.count > 1){
                for ( let index = 0; index < element.count ; index++) {
                  body.fotos.push({
                    idfoto: element.fotoid,
                    tipoentrega: element.idtipoentrega
                  })
                }
              }else{
                body.fotos.push({
                  idfoto: element.fotoid,
                  tipoentrega: element.idtipoentrega
                })
              }
            }
          //}
        }

    })

    console.log("------------BODY FOTOS-----------");
    this.logservice.accion.push(" -- BODY FOTOS numero : "+body.fotos.length+" ");
    
    console.log(body.fotos);



    this.logservice.accion.push(" -- PANEL CARRITO HACIENDO PEDIDO :  ");
    
    this.http.request(
      this.api.baseUrl + 'sales/guardarpedido',
      {
        body: body,
        method: RequestMethod.Post,
        headers: headers,
        withCredentials: false
      }
    )
      .map(response => response.json())
      .map(data => {
          return data;
      }).subscribe(response => {
        console.log(response)
        if(response.Code == '0'){
          _ctx.notificarCompraRealizada(response.CodigoPedido)
          this.carrito.fotosMultiple = [];
          this.carrito.items = [];
          this.logservice.accion.push(" -- PANEL CARRITO response.Code = 0   ");
    
        }
        this.cachedCorreo = ""

      }, (error) => {
          console.log("checkout error");console.error(error);
          //this.carrito.fotosMultiple = [];
          //this.carrito.items = [];
          this.logservice.accion.push(" -- PANEL CARRITO ERROR PEDIDO: "+error+"  ");
          this.translate.get('CART_ALERT_ERROR_TITLE').subscribe((title: string) => {
            this.translate.get('CART_ALERT_ERROR_SUBTITLE').subscribe((subtitle: string) => {
              _ctx.presentAlert(title, subtitle)
              return;
            })
          })
      }
    );
    // this.presentAlert()
  }

  verEnModal(itemCarrito){
    console.log("Ver elemento del carrito en modal")
    let modal = this.modalCtrl.create( ModalFotoFullPage, { 'itemCarrito' : itemCarrito })
    modal.present()
  }

  presentAlert(title,subtitle) {
    let alert = this.alertCtrl.create({
      title: title,
      message: subtitle,
      buttons: ['Ok']
    });
    alert.present();
  }

  limpiarCarrito(){
    this.carrito.vaciar()
    this.carritoVaciado.emit(true)
    this.fotosCarritoSele = [];
    this.testigoseleccion=[];
    this.fotosOriginal = [];
    this.logservice.accion.push(" -- PANEL CARRITO limpiarCarrito:   ");
          
  }

  QuitarUnoCarrito(seleccion){
    console.log("Quitar uno selecccion");
    this.logservice.accion.push(" -- PANEL CARRITO Quitar uno selecccion   ");
    
   if(seleccion == 1){
    this.logservice.accion.push(" -- PANEL CARRITO Quitar uno selecccion seleccion = 1 ");
    
     this.carrito.items.forEach( element => {
         if(element){
           for(var i = 0 ;  i < element.count ; i++){
             console.log("prueba -- element.fotoid "+element.fotoid);
             if(element.fotoid == 1123987){ // quiere decir que hay una seleccion multiple de algun metodo
                element.count = element.count - 1;
             }
           }
         }
       });
   }else{
        this.logservice.accion.push(" -- ELSE -- "+seleccion+" ");
    
        this.carrito.removerCarrito(seleccion)
        if(seleccion.fotoid == 1123987){
          console.log("aNTES DE EMITIR");
          this.carritoVaciado.emit(1002)
          this.quitarTodas.emit(true)
        }
      }
    //this.carrito.vaciar()
    //this.carritoVaciado.emit(true)
  }

  autorizacion(){
    this.logservice.accion.push(" -- PANEL CARRITO autorizacion --  ");
    console.log("hola bb como has estado");
    let modalA = this.modalCtrl.create( ModalAutorizar)
    modalA.present()
  }

  guardarlog(code){

    var body = {
      "code": ""+code+"",
      "log": this.logservice.accion.toString(),
  }
  
  let headers = new Headers();
  headers.append("Content-Type", "application/x-www-form-urlencoded");

    this.http.request(
      this.api.baseUrl + 'sales/guardarlogusuario',
      {
        body: body,
        method: RequestMethod.Post,
        headers: headers,
        withCredentials: false
      }
    )
      .map(response => response.json())
      .map(data => {
          return data;
      }).subscribe(response => {
        console.log(response)
        if(response.Code == '0'){
          console.log("response 0 TODO OK");
          
        }
        this.cachedCorreo = ""

      }, (error) => {
          console.log("checkout error");console.error(error);
          //this.carrito.fotosMultiple = [];
          //this.carrito.items = [];
         
      }
    );
    this.logservice.accion = [];
  }

  opc(){
    console.log("Abriendo modal para otras opciones");
    let modalD = this.modalCtrl.create( ModalDuplicarPage, {"items":this.fotosCarritoSele, "fullItem":this.fotosOriginal} )
    modalD.present()
   
  }

  selecFoto(seleccion){
    if(seleccion.Bol){
       //seleccion.Bol = false;
       this.fotosCarritoSele.push(seleccion.ref);
       this.fotosOriginal.push(seleccion);
       this.testigoseleccion = true;
      
    }else{
      //seleccion.Bol = true;
      
      this.fotosCarritoSele.forEach((element,index) => {
        if(seleccion.fotoid == element.idfoto){
          this.fotosCarritoSele.splice(index,1)
        } 
      });
      if(this.fotosCarritoSele.length == 0){
        this.testigoseleccion = false;
      }

    }
    console.log("funcion selecFoto");
    console.log(seleccion);
  }

}
