import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { PresentacionPage } from '../presentacion/presentacion';
import { GaleriaService } from '../../services/galeria-service';
import { EventosServerService } from '../../services/eventos-server-service';
import { AlmacenamientoService } from '../../services/almacenamiento-service';
import { ModalMetodoEntregaPage } from '../modal-metodo-entrega/modal-metodo-entrega';
import { ModalFotoFullPage } from '../modal-foto-full/modal-foto-full';
import { CarritoService } from '../../services/carrito-service';
import { LogService } from '../../services/logs-service'


/**
 * Generated class for the GaleriaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-galeria',
  templateUrl: 'galeria.html',
  providers : [EventosServerService, GaleriaService]
})
export class GaleriaPage {

  zona: any;
  fotos = []
  fotosTodas = []
  idsFotos = []
  itemV: any; // item para Video
  itemT: any; // item para todo
  fotoSelec: any; //foto seleccionada
  filtro:any; //true or false para el filtro
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public eventosServer: EventosServerService, // unica instanca para galeria
    public almacenamiento: AlmacenamientoService,
    private galeria: GaleriaService,
    public modalCtrl: ModalController,
    public carrito: CarritoService,
    public logservice: LogService
  ) {

    let _ctx = this
    console.log(">>>>>>>>> accediendo a variables");
    this.zona = this.navParams.get('zona');
    this.filtro = this.navParams.get('filtro');
    this.logservice.accion.push("-- En galeria:  zona: "+this.zona+" filtro: "+this.filtro+"");
    if(!this.zona.idhorario){
      if(this.navParams.get('hora') > 0){
        this.zona.idhorario = this.navParams.get('hora');
     }
  }
  if(this.navParams.get('fecha') != "ff"){
    this.galeria.currentDate = this.navParams.get('fecha');
  }
    
    this.galeria.zona = this.zona;
    //this.galeria.filtro = true;//this.filtro;
    // this.galeria.inicializarArrayMinaturas()

  }
  comprarVideo(){
    console.log("comprar video")
    this.itemV = { "miniatura" : "assets/imgs/Video-Icon-PNG-Free-Download.png",
    "slider" : "assets/imgs/Video-Icon-PNG-Free-Download.png",
    "idfoto": 1000022,
    "idzona": 1,
    "selected": false }
    this.carrito.agregarMultiple(this.itemV, 4, 1);
    this.logservice.accion.push("-- COMPRO VIDEO ---");
  }

  comprarTodo(){
    console.log("seleccion all fotos");
    this.itemT = {
        "miniatura" : "assets/imgs/select-512.png",
        "slider" : "assets/imgs/select-512.png",
        "idfoto": 1123987,
        "idzona": 1,
        "selected": false
    }

    this.galeria.selectMultiple = this.itemT;
    this.verModal();
    this.logservice.accion.push(" >>> COMPRA TODO ---");
    return ;

  }

  preseleccion(){
    this.galeria.prese = true;
    this.carrito.fotosMultiple = [];
    this.galeria.presecarray = [];
    this.carrito.items = [];
    this.carrito.vaciar();
    this.logservice.accion.push(" >>> preseleccion vaciado de todo ---");
  }


  irAModoPresentacion(index, item){
    console.log(index);
    this.logservice.accion.push(" >>> ir a modo de presentacion con index: "+index+" galeria.seleccion: "+this.galeria.seleccion+" --- item.selected "+item.selected+" this.galeria.prese: "+this.galeria.prese+"");
    if(this.galeria.seleccion == true){
      console.log("---- es true -----");
      if(item.selected == true){
          item.selected = false;
          this.removeFotoMultiple(item);
      }else{
         item.selected = true;
         this.carrito.fotosMultiple.push(item);
         console.log(this.carrito.fotosMultiple);
         this.carrito.agregarCarritoUno();
         console.log(item);
      }

      return ;
    }else if(this.galeria.prese == true){
             if(item.selected == true){
               item.selected = false;
               this.galeria.removeArray(item);
             }else{
               item.selected = true;
               this.galeria.presecarray.push(item);
             }
    }else{
    console.log(this.galeria.seleccion);
    this.navCtrl.push(PresentacionPage, {index : index, galeria: this.galeria})
    }
  }

  ionViewDidLoad() {
    let _ctx = this
    console.log('ionViewDidLoad GaleriaPage');
    // iniciar el proceso de leer cuantas tengo disponibles,
    // leer nueva imagen y guardarla en local
    // notificar al ws
    this.eventosServer.empezarAPedirFotos(this.galeria,this.zona)
  }

  verModal(){
    console.log("ver modal para metodo de entrega de todas las fotos");
    let modal = this.modalCtrl.create( ModalMetodoEntregaPage, { "galeria" : this.galeria })
    modal.present()
  }

  carritoVaciado(value){
    this.logservice.accion.push(" >>> carrito vaciado: --");
    
    console.log("---- CARRITO VACIADO ----")
    this.galeria.seleccion = false;
    this.galeria.miniaturasTest.forEach(value=>{
       value.selected = false
      }
     )
    if(value == 1002){
          console.log("---- SELECCION FAlse ----")
      this.galeria.seleccion = false;
      this.galeria.miniaturasTest.forEach(value=>{
         value.selected = false
        }
       )
     this.galeria.presecarray = [];  
     this.galeria.selectMultiple = [];
     this.logservice.accion.push(" >>> carrito vaciado: value = 1002 --"+this.galeria.seleccion+" geleria.presecarray length: "+this.galeria.presecarray.length+" galeria.selectMultiple length: "+this.galeria.selectMultiple+"");
     
    }
    //this.settearIconosMetodosEntrega(false)
  }

  quitarTodas(value){
    console.log("---- QUITAR TODAS ----")
    if(value == true){
      this.galeria.seleccion = false;
      this.galeria.presecarray = [];
      this.galeria.selectMultiple = [];
    }
    this.logservice.accion.push(" >>> quitar todas -- value"+value+"");
      
  }

  holding(item){

    console.log("hoooldin")
    let modalFull = this.modalCtrl.create( ModalFotoFullPage, { "itemCarrito" : item, "testigo": 1 })
    modalFull.present()
  }

  //remove de array fotos selec fotosMultiple
  removeFotoMultiple(item){
   this.carrito.fotosMultiple.forEach((foto, index) => {
     if(foto.idfoto == item.idfoto){
        this.carrito.fotosMultiple.splice(index,1)
     }
   });
   this.carrito.removerCarritoUno();
   console.log("menos uno ");
   console.log(this.carrito.fotosMultiple);
   this.logservice.accion.push(" >>> removerCarritoUno menos uno");

}

irAModoPresentacionPRE(index){
  this.logservice.accion.push(" >>> irAModoPresentacionPRE this.navCtrl index: "+index+" ");
  this.navCtrl.push(PresentacionPage, {index : index, galeria: this.galeria})
}

}
