import { Injectable } from '@angular/core';
import {NavParams } from 'ionic-angular';
import { FotobruApiService } from './fotobru-api-service';
import { GaleriaService } from './galeria-service';
import Config from './config';

@Injectable()
export class CarritoService{

    public items = []
    public count = 0
    public total = 0
    public galeria: any
    public fotosMultiple = [] //fotos ocultas seleccionadas en seleccion multiple
    public metodoMultiple = [] // metodo o metodos de la entrega multiple
    public testigoDuplicar:any; //testigo para metodo duplicar
    public numeroDuplicar = 0 ; //cuando es mayor de 0 quiere decir que se hara una inserccion por el metodo duplicar 
    /**
     *
     */
    constructor(public api: FotobruApiService ) {
      //this.galeria = this.navParams.get('galeria')

    }

    public vaciar(){
        var retValue = false
        this.count = 0
        this.total = 0
        // for (let i= 0; i < this.items.length; i++) {
        //     this.items[i] = false
        // }
        this.items = [];
        this.fotosMultiple = []; //cambia la referencia ? // no en navegador!
        this.metodoMultiple = [];
    }

    public pagePresentacion: any


    /**
     * Calcula el total de items que hay en el carrito para cada metodo de entrega
     * Devuelve un objeto con el total para cada metodo de entrega.
     * @returns obj { correo : totalCorreo , impresora: totalImpresora, usb: totalUsb , cd: totalCd, video: 1 }
     */
    private calcularTotalesMetodosDeEntreta(){
        console.log('En metodo calcularTotalesMetodosDeEntreta() ...')

        var correo = 0 ,impresora = 0 ,usb = 0, cd = 0, video = 0
        this.items.forEach(element => {
            if(element){
            console.log(element)
            if(element.idtipoentrega == 1)
                correo += element.count;
            if(element.idtipoentrega == 2)
                impresora += element.count;
            if(element.idtipoentrega == 3)
                usb += element.count;
            if(element.idtipoentrega == 4)
                cd += element.count;
            if(element.fotoid=== 1000022)
               video = 1    
            }
        })

        var objeto = {
            correo : correo,
            impresora: impresora,
            usb: usb,
            cd: cd,
            video: video
        }
        // console.log('OBJETO = ')
        // console.log(objeto)
        return objeto
    }

    private insertaORemove(item, method){
      console.log("Calculando totales");
      let _ctx = this
      var correo = 0 ,impresora = 0 ,usb = 0, cd = 0, video =0
      if(this.numeroDuplicar > 0){
            if(this.testigoDuplicar){
                this.pagePresentacion.mostrarCargando()
                this.testigoDuplicar = false;
            }
        }else{
            if(this.testigoDuplicar){
                this.testigoDuplicar = false;
            }
            if(this.pagePresentacion != null){
                this.pagePresentacion.mostrarCargando()     
            }
        }  
      

      // CALCULA EL TOTAL DE ITEMS QUE HAY POR CADA METODO DE ENTREGA
      let totales = this.calcularTotalesMetodosDeEntreta()
      correo = totales.correo
      impresora = totales.impresora
      usb = totales.usb
      cd = totales.cd
      video = totales.video

      // DECREMENTA O INCREMENTA EN 1
      if(method == 'remove'){

        if(this.items[item].idtipoentrega == 1)
            correo -= this.items[item].count;
        if(this.items[item].idtipoentrega == 2)
            impresora -= this.items[item].count;
        if(this.items[item].idtipoentrega == 3)
            usb -= this.items[item].count;
        if(this.items[item].idtipoentrega == 4)
            cd -= this.items[item].count;

        if(this.items[item].fotoid == 1000022){
            video = video - 1;
        }    

      } else {

        if(item.idtipoentrega == 1){
         if(item.count > 1){
            correo += item.count
         }else{
            correo += 1;   
         }
        } 
        if(item.idtipoentrega == 2){
            if(item.count > 1){
                impresora += item.count
            }else{
                impresora += 1;   
            }
        } 
            
        if(item.idtipoentrega == 3){
            if(item.count > 1){
                usb += item.count
            }else{
                usb += 1;   
            }
        }    
        if(item.idtipoentrega == 4){
            if(item.count > 1){
                cd += item.count
            }else{
                cd += 1;   
            }
        } 
        if(item.fotoid == 1000022){
            video +=  1;
        }          

      }

      if(Config.ModoMockData){
        //ASIGNA UN TOTAL
        // precio por unidad es fijo para datos de prueba
        var objeto = {
            correo : correo,
            impresora: impresora,
            usb: usb,
            cd: cd
        }
        console.log('MOCK OBJETO = ')
        console.log(objeto)
        _ctx.total = correo * 50 + impresora * 60 + usb * 70 + cd * 80;

        _ctx.insertRemoveInArray(item, method)
        console.log('length of array of items = ' + _ctx.items.length)

        // RECALCULANDO EL TOTAL PARA CADA Fotos
        // precio por foto o subtotal ?????
        this.items.forEach(element => {
            if(element.idtipoentrega == 1)
                element.precio = 50
            if(element.idtipoentrega == 2)
                element.precio = 60
            if(element.idtipoentrega == 3)
                element.precio =  70
            if(element.idtipoentrega == 4)
                element.precio = 80
        })
        if(this.numeroDuplicar == 0){
            console.log("NUMERO DUPLICAR IGUAL A CERO");
            if(this.pagePresentacion != null){
                this.pagePresentacion.quitarCargando()
                this.testigoDuplicar = 0;
            }  
        }
        
       
        return;
      }

      _ctx.api.obtenerApiKey((apikey) => {
        _ctx.api.buildPeticion(
          'sales/costopedido',
          "ApiKey=" + apikey+'&correo='+correo+'&impresas='+impresora+'&usb='+usb+'&cd='+cd+'&video='+video
        ).subscribe( (data: any) => {
          console.log("Obteniendo los totales = ")
          console.log(data)

          _ctx.total = data.PedidosImpreso[0].total


          _ctx.insertRemoveInArray(item, method)
           if(_ctx.numeroDuplicar > 0)
             _ctx.numeroDuplicar = _ctx.numeroDuplicar - 1;
          // RECALCULANDO EL TOTAL PARA CADA Fotos
          _ctx.items.forEach(element => {
            if(element){
              if(element.idtipoentrega == 1)
                element.precio = data.PedidosImpreso[0].totaldigitales / correo
              if(element.idtipoentrega == 2)
                element.precio = data.PedidosImpreso[0].totalimpresas /  impresora;
              if(element.idtipoentrega == 3)
                element.precio = data.PedidosImpreso[0].totalusb /    usb;
              if(element.idtipoentrega == 4)
                element.precio = data.PedidosImpreso[0].totalcd /      cd;

              if(element.fotoid == 1000022)
                element.precio = data.PedidosImpreso[0].totalvideo ;  
            }
          })

          if(this.pagePresentacion != null)
                if(this.numeroDuplicar == 0){
                    this.pagePresentacion.quitarCargando()        
                }
        })
      })
    }

    private insertRemoveInArray(item, method){
      console.log("insertRemoveInArray");
        if(method == 'remove'){
            if(this.items[item].fotoid == 1123987){
               this.count = this.count - 1;
            
             }
            if(this.items[item].fotoid == 1123987){
              console.log("----- elimino seleccion todas las fotos  ------")
              this.items[item] = false
              return true
            }
            this.items[item] = false
            this.count = this.count - 1;
            return false
        }else{
            if(item.nuevo){
                this.items.push(item)
                item.nuevo = false
            }else{
                //this.items.push(item)
                item.count++;
            }
            this.count += 1
        }
    }

    hayImagenConEntregaCorreo(){
        var retValue = false
        for (let i= 0; i < this.items.length; i++) {
            if(this.items[i] && this.items[i].idtipoentrega == 1 )
            return true
        }
        return false
    }
    /**
     * Agrega solo si el elemento no existe
     * @param elemento
     */
    agregar(elemento,metodo){
      console.log("meeetodo")
        var buscada : any = this.buscarAgregadasPorIdYMetodo(elemento.idfoto, metodo.id)
        if(!buscada){

          this.insertaORemove({
                fotoid: elemento.idfoto,
                idtipoentrega: metodo.id,
                metodoImg : metodo.relleno,
                ref: elemento,
                count: 1,
                precio : 1,
                nuevo : true
            }, 'insert')


            //this.total += 70
        } else {
            if(metodo.id == 2)
              this.insertaORemove(buscada,'insert')

            
            if(this.numeroDuplicar > 0){
                this.numeroDuplicar = this.numeroDuplicar - 1;
                if(this.numeroDuplicar == 0){
                    this.pagePresentacion.quitarCargando()   ;     
                }
             }  

        }
    }

    /**
     * Agrega metodo para seleccion multiple
     *
     */

     agregarMultiple(elemento,metodo,numImagenes){
         console.log("nummmmm");
         var buscada : any = this.buscarAgregadasPorIdYMetodo(elemento.idfoto, metodo.id)
         if(!buscada){

           this.insertaORemove({
                 fotoid: elemento.idfoto,
                 idtipoentrega: metodo.id,
                 metodoImg : metodo.relleno,
                 ref: elemento,
                 count: numImagenes,
                 precio : 0,
                 nuevo : true
             }, 'insert')


             //this.total += 70
         } else {
             this.insertaORemove(buscada,'insert')
         }
     }

    /**
     * Remueve un elemento del carrito
     *
     */
    removerCarrito(elementoCarrito){
        // this.removerCarritoMetodo(elementoCarrito , { id : elementoCarrito.idtipoentrega} )
        for (let i= 0; i < this.items.length; i++) {
            if(this.items[i] && this.items[i].fotoid == elementoCarrito.fotoid && this.items[i].idtipoentrega == elementoCarrito.idtipoentrega){

                // this.items[i] = false
                // this.count--;

                this.insertaORemove(i, 'remove')
                // this.total -= 70

                if(this.pagePresentacion != null)
                    this.pagePresentacion.limpiarMetodo(elementoCarrito.fotoid, elementoCarrito.idtipoentrega)
            }
        }
    }

    removerCarritoUno(){
      for (let i= 0; i < this.items.length; i++) {
          if(this.items[i] && this.items[i].fotoid == 1123987){
              this.items[i].count = this.items[i].count - 1; 
              this.calculaCostoTotal();
              // this.items[i] = false
            }
      }
    }

    agregarCarritoUno(){
        for (let i= 0; i < this.items.length; i++) {
            if(this.items[i] && this.items[i].fotoid == 1123987){
                this.items[i].count = this.items[i].count + 1; 
                this.calculaCostoTotal();
                // this.items[i] = false
              }
        }
      }


    removerCarritoMetodo(elemento,metodo){
        for (let i= 0; i < this.items.length; i++) {
            if(this.items[i] &&
                this.items[i].fotoid == elemento.idfoto &&
                this.items[i].idtipoentrega == metodo.id)
            {
                this.insertaORemove(i, 'remove')
                //this.total -= 70

                if(this.pagePresentacion != null)
                    this.pagePresentacion.limpiarMetodo(elemento.idfoto, metodo.id)
            }
        }
    }



    /**
     *  Devuelve muchas segun se hayan agregado a los metodos
     * @param fotoid
     */
    buscarAgregadasPorId(fotoid){
        var buscadas = []
        this.items.forEach(value=>{
            if(value && value.fotoid == fotoid)
                buscadas.push( value )
        })
        console.log(">> BUSCADAS");
        console.log(buscadas);
        return buscadas
    }

    /**
     *  Devuelve 1
     * @param fotoid
     * @param metodo
     */
    buscarAgregadasPorIdYMetodo(fotoid,metodo){
        var buscada = false
        this.items.forEach(value=>{
            if(value && value.fotoid == fotoid && value.idtipoentrega == metodo)
                buscada = value
        })
        return buscada
    }
    /**
     * Calcula el costo total del carrito solamente y regresa el valor
     * 
     */

    calculaCostoTotal(){
        //console.log("calcularCostoTotal")
        let _ctx = this
        
        let totales = this.calcularTotalesMetodosDeEntreta();
        console.log("calcularCostoTotal")
        console.log(totales);
        var correo = 0 ,impresora = 0 ,usb = 0, cd = 0, video =0
        
        correo = totales.correo
        impresora = totales.impresora
        usb = totales.usb
        cd = totales.cd
        video = totales.video


        _ctx.api.obtenerApiKey((apikey) => {
            _ctx.api.buildPeticion(
              'sales/costopedido',
              "ApiKey=" + apikey+'&correo='+correo+'&impresas='+impresora+'&usb='+usb+'&cd='+cd+'&video='+video
            ).subscribe( (data: any) => {
              console.log("Obteniendo los totales = ")
              console.log(data)
    
              _ctx.total = data.PedidosImpreso[0].total
    
              // RECALCULANDO EL TOTAL PARA CADA Fotos
              _ctx.items.forEach(element => {
                if(element){
                  if(element.idtipoentrega == 1)
                    element.precio = data.PedidosImpreso[0].totaldigitales / correo
                  if(element.idtipoentrega == 2)
                    element.precio = data.PedidosImpreso[0].totalimpresas /  impresora;
                  if(element.idtipoentrega == 3)
                    element.precio = data.PedidosImpreso[0].totalusb /    usb;
                  if(element.idtipoentrega == 4)
                    element.precio = data.PedidosImpreso[0].totalcd /      cd;
    
                  if(element.fotoid == 1000022)
                    element.precio = data.PedidosImpreso[0].totalvideo ;  
                }
              })
    
            })

        })
   }
}
