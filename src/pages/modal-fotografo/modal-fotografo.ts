import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { GruposService } from '../../services/grupos-service';

/**
 * Generated class for the ModalClientePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-cliente',
  templateUrl: 'modal-cliente.html',
})
export class ModalFotografoPage {

  public data = {
    nombre : ''
  }

  public fotografos = [];
  public fotografoSelect: any;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public viewCtrl: ViewController, public navParams: NavParams, public translate: TranslateService, public grupoService: GruposService ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalFotografosPage');
    this.grupoService.getFotografos();
   this.fotografos = this.grupoService.fotografos ;
  }

  
  actionCancelar(){

    this.viewCtrl.dismiss(null);
  }
}
