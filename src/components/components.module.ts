import { NgModule } from '@angular/core';
import { PedidosNavbarComponent } from './pedidos-navbar/pedidos-navbar';
import { IonicModule } from 'ionic-angular';
import { PanelCarritoComponent } from './panel-carrito/panel-carrito';
import { WrapperPanelCarritoComponent } from './wrapper-panel-carrito/wrapper-panel-carrito';
import { TranslateModule } from '@ngx-translate/core';
import { DragScrollModule } from 'ngx-drag-scroll';

@NgModule({
	declarations: [
		PedidosNavbarComponent,
		PanelCarritoComponent,
		WrapperPanelCarritoComponent
	],
	imports: [
		IonicModule,
		TranslateModule.forChild({
			isolate: false
		}),
		DragScrollModule
	],
	exports: [
		PedidosNavbarComponent,
		PanelCarritoComponent,
		WrapperPanelCarritoComponent
	]
})
export class ComponentsModule {}
