import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ModalFotoFullPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-foto-full',
  templateUrl: 'modal-foto-full.html',
})
export class ModalFotoFullPage {

  public itemCarrito : any
  public itemAux: any
  public testigo: any
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    if(navParams.get('testigo')){
      
      this.testigo = 5
      this.itemAux = navParams.get('itemCarrito');
      //this.itemCarrito.ref.slider = this.itemAux.miniatura
    }else{
      this.testigo = 0
      this.itemCarrito = navParams.get('itemCarrito')
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalFotoFullPage');
  }

}
