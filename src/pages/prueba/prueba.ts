import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GaleriaPage } from '../galeria/galeria';
import { GruposService } from '../../services/grupos-service';
import { DatePicker } from '@ionic-native/date-picker';
import { TranslateService } from '@ngx-translate/core';
import { LogService } from '../../services/logs-service'



/**
 * Generated class for the PruebaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-prueba',
  templateUrl: 'prueba.html',
})
export class PruebaPage {

  public myDate: any;
  public horario:any;
  public zona:any;
  public filtro=false;
  public buscartes=false;


  constructor(public navCtrl: NavController, public gruposSrv: GruposService, public datePicker: DatePicker, public translate: TranslateService, public logservice: LogService ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PruebaPage');
    console.log("horarios  ");
    console.log(this.gruposSrv.horarios);
    this.gruposSrv.testigofiltro = false;
    this.gruposSrv.getvalores()

    }

  irAGaleria(item){
    console.log("ir a galeria "); 
    
    this.navCtrl.push(GaleriaPage, {zona : item, filtro : this.filtro, hora: this.gruposSrv.hr, fecha: this.gruposSrv.ff });
    this.logservice.accion.push("/> Hacia Galeria con: zona: "+item.zona+" idz: "+item.idzona+" filtro: "+this.filtro+" hora: "+this.gruposSrv.hr+" fecha "+this.gruposSrv.ff+" //");
  }
  settearIpLocal(){
      this.gruposSrv.reload()
  }
  testbutton(){

  }

  dia(){

  }

  buscar(){
    console.log("la fecha es "+this.myDate);
    console.log("el horario es "+this.horario);
    console.log("la zona es "+this.zona);
    this.filtro = true;
    
    this.gruposSrv.testigofiltro = true;
    this.gruposSrv.reload();
    this.buscartes = true;
    this.gruposSrv.grupos = [];
    this.gruposSrv.zonasfiltro(this.myDate,this.horario,this.zona)
    
  }

  cancelar(){
    this.myDate = "";
    this.horario = "";
    this.zona = "";
    this.filtro = false;
    this.buscartes = false;
    this.gruposSrv.testigofiltro = false;
    this.gruposSrv.reload();
    this.gruposSrv.hr = 0;
    this.gruposSrv.zna = 0;
    this.gruposSrv.ff = "";
  }


}
