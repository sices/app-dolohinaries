import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalDatosTargetaPage } from './modal-datos-targeta';

@NgModule({
  declarations: [
    ModalDatosTargetaPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalDatosTargetaPage),
  ],
})
export class ModalDatosTargetaPageModule {}
