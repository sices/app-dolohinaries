import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GaleriaPage } from '../galeria/galeria';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  grupos : any;

  constructor(public navCtrl: NavController) {
    this.grupos = [ ];

    for( let index = 0 ;  index < 4 ; index++){
      const element = {
          title: "Grupo " + 1,
          imagenes: [
            // 'assets/imgs/node.png',
            // 'assets/imgs/cuponeate.png',
            // 'assets/imgs/logo.png',
            // 'assets/imgs/globaloxs.png'
            'assets/imgs/elrollo.jpg',
            'assets/imgs/elrollo.jpg',
            'assets/imgs/elrollo.jpg',
            'assets/imgs/elrollo.jpg'
          ],
      }
      this.grupos.push(element)
    }

  }

  irAGaleria(){
    this.navCtrl.push(GaleriaPage);
  }

}
