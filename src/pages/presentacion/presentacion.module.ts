import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PresentacionPage } from './presentacion';
import { ComponentsModule } from '../../components/components.module';
import { DragScrollModule } from 'ngx-drag-scroll';

@NgModule({
  declarations: [
    PresentacionPage,
  ],
  imports: [
    IonicPageModule.forChild(PresentacionPage),
    ComponentsModule,
    DragScrollModule
  ],
})
export class PresentacionPageModule {}
