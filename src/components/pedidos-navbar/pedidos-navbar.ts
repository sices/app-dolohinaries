import { Component, EventEmitter, Output } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { ConfiguracionesPage } from '../../pages/configuraciones/configuraciones';
import { TranslateService } from '@ngx-translate/core';
import { FotobruApiService } from '../../services/fotobru-api-service';

/**
 * Generated class for the PedidosNavbarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'pedidos-navbar',
  templateUrl: 'pedidos-navbar.html'
})
export class PedidosNavbarComponent {

  text: string;

  constructor(public modalCtrl: ModalController, public translate: TranslateService,public api: FotobruApiService) {
    console.log('Hello PedidosNavbarComponent Component');
  }

  abrirConfiguraciones(){
    let _ctx = this
    let modal = this.modalCtrl.create( ConfiguracionesPage )

    modal.onDidDismiss(data => {
        _ctx.api.localIp = data
        _ctx.cambioIp.emit(data)
    });
        modal.present()

  }
  switchIdioma(idioma){
    this.translate.use(idioma)

  }

  @Output() compraRealizada: EventEmitter<any> = new EventEmitter();
  @Output() carritoVaciado: EventEmitter<any> = new EventEmitter();
  @Output() cambioIp: EventEmitter<any> = new EventEmitter();

  privateEvent(value){
    console.log('En pedidos navbar, propagando la emision del evento, value = ' + value)
    this.compraRealizada.emit(value)
  }

  privateEvent2(value){
    console.log('En pedidos navbar, propagando la emision del evento, value = ' + value)
    this.carritoVaciado.emit(value)
  }

}
