import { Injectable, Pipe } from '@angular/core';
import 'rxjs/add/operator/map';
import { FotobruApiService } from './fotobru-api-service';
import { AlmacenamientoService } from './almacenamiento-service';
import Config from './config';

@Injectable()
export class GruposService {
    /**
     * Array de la siguiente forma, p/e
     * [
     *  {
     *    idzona: 1
     *    zona: "zona 1"
     *    fotos: []
     *  },
     *  ....
     * ]
     *
     */
    public today = new Date();
    //split
    public dd = this.today.getDate();
    public mm = this.today.getMonth() + 1; //because January is 0! 
    public yyyy = this.today.getFullYear();

    public currentDate = this.yyyy+"-"+this.mm+"-"+this.dd;

    public grupos = [];
    public horarios = [];
    public zonasbase = [];
    public fotografos= [];
    public navCtrl : any;
    public testigofiltro:any;
    public ff="aa";
    public hr=0;
    public zna=0; 
    public codigofotografo = ""; //clave para fin de venta
    /**
     *
     */
    constructor(public api: FotobruApiService, public almacenamiento: AlmacenamientoService) {
      let _ctx = this
      var actualizar = this.reload.bind(this)
      setInterval(function(){ actualizar(); }, 20000);
     
      this.reload();
      
    }


    public reload(){
      
      console.log("RECARGANDO ....  con la ipe del usuario = " + this.api.localIp)
      if( Config.ModoMockData){
        this.settearDatosPrueba()
      }else{
        if(this.testigofiltro == false){
        this.grupos = []
        this.api.obtenerApiKey( this.leerDatosDeWS.bind(this) )
         
        this.checkserver();
        }
      }
    }

    public zonasfiltro(fecha,horario,zona){
      console.log("En zonas filtro "+fecha+horario+zona);
      this.ff = fecha;
      this.hr = horario;
      this.zna = zona; 
      this.api.obtenerApiKey( this.zonasfiltrodos.bind(this) )

    }

    public zonasfiltrodos(apikey){
      console.log("En zonas filtro DOS");
      let _ctx = this
      let finalhr:any;
      let finalzna:any;
      //let apikey = "313232132";
      if(!_ctx.hr){
        finalhr = "";
      }else{
        finalhr = _ctx.hr;
      }

      if(!_ctx.zna){
        finalzna = "";
      }else{
        finalzna = _ctx.zna;
      }
      this.api.buildPeticion(
        'sales/zonasfiltro',
        "ApiKey="+apikey+'&fecha='+_ctx.ff+'&horario='+finalhr+'&zona='+finalzna
      )
      .subscribe((data:  any) => {
        console.log("en subscribe method, data = ")
        console.log(data)
        data.Zonas.forEach( zona => {
          zona.fotos = []
          _ctx.grupos.push( zona )
          // if( zona.idzona ==1 || zona.idzona == 2  || zona.idzona == 3 || zona.idzona == 5) //TODO: QUITAR AL FINALIZAR EL DESARROLLO
            _ctx.previewsZonaFiltro(apikey, zona.idzona, zona.idhorario)
        })
      })
    }

    public checkserver(){
      console.log("checkserver RECARGANDO ....")
      if( Config.ModoMockData){
        
      }else{
        this.api.checkServer()
      }
    }

    settearDatosPrueba(){
      let url1 = 'https://media-cdn.tripadvisor.com/media/photo-s/04/37/86/ce/el-rollo-parque-acuatico.jpg'
      let url2 = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRqSaj4ASKDpndgHqwG15QMQL6uHSv3FMBGb-9wpvldNpR661wH'
      this.grupos.push({ idzona : 1, zona: "zona 1", fotos : [ url1, url1, url1, url1 ], })
      this.grupos.push({ idzona : 2, zona: "zona 2", fotos : [ url2, url2, url2, url2 ], })
    }

    leerDatosDeWS(apikey){
        let _ctx = this
        console.log(".-..-.-.-.-.-.-.-.-.-.-.-.-.-.-.-..-.-.-.-") 
        this.api.buildPeticion(
          'sales/zonas',
          "ApiKey=" + apikey
        )
        .subscribe((data:  any) => {
          console.log("en subscribe method, data = ")
          console.log(data)
          data.Zonas.forEach( zona => {
            zona.fotos = []
            _ctx.grupos.push( zona )
            // if( zona.idzona ==1 || zona.idzona == 2  || zona.idzona == 3 || zona.idzona == 5) //TODO: QUITAR AL FINALIZAR EL DESARROLLO
            _ctx.hr = zona.idhorario;  
            this.ff = this.currentDate;
            console.log("GRUPOSSS "+_ctx.grupos);
            _ctx.previewsZonaFiltro(apikey, zona.idzona, zona.idhorario)
          })
        })
    }

    
    leerDatosDeWSFiltros(fecha,horario,zona){
      console.log(" LEER DATOSFILTROS UNO ")
      this.api.obtenerApiKey( this.leerDatosDeWSFiltrosDos.bind(this,fecha,horario,zona) )
      /*let _ctx = this
      let apikey = "313232132";
      this.api.buildPeticion(
        'sales/zonasimagenesfiltro',
        "ApiKey="+apikey+'&fecha='+fecha+'&horario='+horario+'&zona='+zona
      )
      .subscribe((data:  any) => {
        console.log("en subscribe method, data = ")
        console.log(data)
        data.Zonas.forEach( zona => {
          zona.fotos = []
          _ctx.grupos.push( zona )
          // if( zona.idzona ==1 || zona.idzona == 2  || zona.idzona == 3 || zona.idzona == 5) //TODO: QUITAR AL FINALIZAR EL DESARROLLO
            _ctx.previewsZona(apikey, zona.idzona)
        })
      })*/
  }

  leerDatosDeWSFiltrosDos(apikey,fecha,horario,zona){
    console.log(" LEER DATOSFILTROS DOS ")
        
    let _ctx = this
    this.api.buildPeticion(
      'sales/zonasimagenesfiltro',
      "ApiKey="+apikey+'&fecha='+fecha+'&horario='+horario+'&zona='+zona
    )
    .subscribe((data:  any) => {
      console.log("en subscribe method, data = ")
      console.log(data)
      data.Zonas.forEach( zona => {
        zona.fotos = []
        _ctx.grupos.push( zona )
        // if( zona.idzona ==1 || zona.idzona == 2  || zona.idzona == 3 || zona.idzona == 5) //TODO: QUITAR AL FINALIZAR EL DESARROLLO
          _ctx.previewsZona(apikey, zona.idzona)
      })
    })
  }

    previewsZona(apikey, idzona){

      let _ctx = this

          _ctx.pedirMinisHomeAlWS(apikey, idzona)
    }

    previewsZonaFiltro(apikey, idzona, idhorario){
      console.log("previeZF "+idzona+" "+idhorario);
      let _ctx = this
 
          _ctx.pedirMinisHomeAlWSFiltro(apikey, idzona, idhorario)
    }

    asignarMinis(idzona, arrayImgsBase64, idhorario){
      console.log("asignar zonas")
      let prefix = "data:image/jpg;base64,";
      let _ctx = this
      let testigo=0; //nos ayuda a solo hacerlo una vez y no varias caso zona repetido 3 veces
      //busco la zona
      _ctx.grupos.forEach(zona =>{
        if(zona.idzona==idzona && zona.idhorario == idhorario){
          // inserto las imagenes
         
              console.log("FOREACH"+zona );
              arrayImgsBase64.fotos.forEach(base64foto => {
              zona.fotos.push( prefix + base64foto)
            })
         
        }
      })
      
    }

    pedirMinisHomeAlWS(apikey, idzona){
        console.log('Obteniendo las minis del home para la zona '+ idzona)
        let _ctx = this

        this.api.buildPeticion(
          'sales/zonasimagenes',
          "ApiKey=" + apikey+'&idZona='+idzona,
        )
        .subscribe( (data : any) => {
          _ctx.asignarMinis(idzona, data, 0)
        })
    }

    pedirMinisHomeAlWSFiltro(apikey, idzona, idhorario){
      console.log('Obteniendo las minis del home para la zona FILTRO '+ idzona+ " "+idhorario)
      let _ctx = this
      let realhr:any; 
      if(idhorario == 999){
        realhr = "";
      }else{
        realhr = idhorario;
      }
      /* if(this.hr == 0 ){
        realhr = ""
       }else if(!this.hr){
        realhr = "";
       }else{
        realhr = this.hr;
       }*/

      this.api.buildPeticion(
        'sales/zonasimagenesfiltro',
        "ApiKey=" + apikey+'&idZona='+idzona+'&idHorario='+realhr+'&fecha='+this.ff,
      )
      .subscribe( (data : any) => {
        _ctx.asignarMinis(idzona, data, idhorario)
      })
  }
    getvalores(){
      this.api.obtenerApiKey( this.valoresBase.bind(this) )
    }

    valoresBase(apikey){
      //console.log('Obteniendo las minis del home para la zona '+ idzona)
      let _ctx = this
       
      this.api.buildPeticion(
        'sales/horarios',
        "ApiKey=" + apikey,
      )
      .subscribe( (data : any) => {
         console.log(data);
         data.Horarios.forEach(horario => {
           this.horarios.push(horario);
         })
        
      })

      this.api.buildPeticion(
        'sales/allzonas',
        "ApiKey=" + apikey,
      )
      .subscribe( (data : any) => {
         console.log(data);
         data.Zonas.forEach(zona => {
           this.zonasbase.push(zona);
         })
        
      })



  }

  
  getFotografos(){
    this.api.obtenerApiKey( this.Fotografo.bind(this) )
  }


  Fotografo(apikey){
    console.log('Obteniendo los FOTOGRAFOS')
    let _ctx = this
     
    this.api.buildPeticion(
      'sales/fotografos',
      "ApiKey=" + apikey,
    )
    .subscribe( (data : any) => {
       console.log(data);
       data.Usuarios.forEach(fotografos => {
         this.fotografos.push(fotografos);
       })
      
    })

}

validafotografo(){
  console.log('Obteniendo los FOTOGRAFOS')
  let _ctx = this
   
  this.api.buildPeticion(
    'sales/validausuario',
    "password=" + this.codigofotografo,
  )
  .subscribe( (data : any) => {
    console.log("ID DEL FOTOGRAFO"+data);
      
    if(data.code == 1)
        return data.code;

    return data.idusuario; 
    
  })

}



}
