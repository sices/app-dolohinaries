import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { CarritoService } from '../../services/carrito-service';
import { GaleriaService } from '../../services/galeria-service';
import { LogService } from '../../services/logs-service'


/**
 * Generated class for the ModalFotoFullPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-duplicar',
  templateUrl: 'modal-duplicar.html',
})
export class ModalDuplicarPage {

  public itemsCarrito = [];
  public itemsOriginal = [];
  public seleccion : any
  public testigo: any //nos dices si hay seleccion en galeria
  public rturn: any
  metodosEntregaStates = [
   /* {
      icon: "assets/imgs/icons/usb-stroke.png",
      relleno: "assets/imgs/icons/usb-relleno.png",
      selected: false,
      method: 'usb',
      id: 3
    },*/
    {
      icon: "assets/imgs/icons/mail-stroke.png",
      relleno:"assets/imgs/icons/mail-relleno.png",
      selected: false,
      method: 'mail',
      id: 1
    },
    {
      icon:"assets/imgs/icons/impresora-stroke.png",
      relleno:"assets/imgs/icons/impresora-relleno.png",
      selected: false,
      method: 'impresora',
      id:2
    },
    {
      icon:"assets/imgs/icons/cd-stroke.png",
      relleno:"assets/imgs/icons/cd-relleno.png",
      selected: false,
      method: 'cd',
      id: 4
    }


  ];

  methods = ['usb','mail','impresora','cd']
  galeria: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public carrito: CarritoService, public viewCtrl: ViewController, public logservice: LogService) {
    //this.galeria = navParams.get('galeria');
    this.itemsCarrito = this.navParams.get('items');
    this.itemsOriginal = this.navParams.get('fullItem');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalMetodoEntregaPage');
    this.logservice.accion.push(" ++ en modal metodo entraga ");
    
  }

  agregarAMetodo(item){
      //let currentIndex = this.slides.getActiveIndex();
      console.log("agregar a metodo ......");
      //this.carrito.pagePresentacion = null;
      
      this.carrito.numeroDuplicar = this.itemsCarrito.length;
      this.carrito.testigoDuplicar = true;
      this.itemsCarrito.forEach(element => {
        this.carrito.agregar( element, item )  
      });
      /*this.itemsOriginal.forEach(element => {
        element.selected = false;
      });*/
      this.itemsOriginal.forEach(some => {
        some.Bol = false;
      });
      
      this.itemsOriginal = [];
      this.itemsCarrito = [];
      //let num = this.itemsCarrito.length * 2;
      
      
      this.viewCtrl.dismiss();
      
    }

    /*okei(){
      this.viewCtrl.dismiss();
      this.logservice.accion.push(" click en modal entrega OK ");
    
    }
    
    finalizarSeleccion(){
      this.logservice.accion.push(" click en modal finalizarSeleccion MODAL ");
      this.galeria.seleccion = false;
      this.viewCtrl.dismiss();
    }*/

}
