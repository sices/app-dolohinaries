import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { GruposService } from '../../services/grupos-service';
import { FotobruApiService } from '../../services/fotobru-api-service';
import { LogService } from '../../services/logs-service'


/**
 * Generated class for the ModalClientePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-cliente',
  templateUrl: 'modal-cliente.html',
})
export class ModalClientePage {

  public result:any;
  public data = {
    nombre : '',
    codigo : ''
  }

  public fotografo = "";

  public fotografos = [];
  constructor(public api: FotobruApiService ,public navCtrl: NavController, public alertCtrl: AlertController, public viewCtrl: ViewController, public navParams: NavParams, public translate: TranslateService, public grupoService: GruposService, public logservice: LogService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalClientePage');
    this.grupoService.getFotografos();
    this.fotografos = this.grupoService.fotografos ;
    this.logservice.accion.push(" -- En MODAL CLIENTE ");
  }

  actionvalidar(){
    console.log("ANTES DE VALIDADOOOO ");

    if(this.fotografo.length == 0){

      this.translate.get('MODAL_CLIENTE_ALERT_TITLE').subscribe((title: string) => {
      this.translate.get('MODAL_CLIENTE_ALERT_TEXT').subscribe((subtitle: string) => {
        let alert = this.alertCtrl.create({
          title: title,
          message: subtitle,
          buttons: ['Ok']
        });
        alert.present();
      });
      });
      return;
    }

    this.logservice.accion.push(" -- VALIDANDO CODIGO DE FOTOGRAFO: "+this.fotografo);
    this.api.buildPeticion(
      'sales/validausuario',
      "password=" + this.fotografo,
    )
    .subscribe( (data : any) => {
      console.log("ID DEL FOTOGRAFO"+data);
        
      if(data.code == 1){
        this.logservice.accion.push(" -- CODIGO NO VALIDO -- ");
        this.translate.get('MODAL_CLIENTE_ALERT_TITLE').subscribe((title: string) => {
          this.translate.get('MODAL_CLIENTE_ALERT_TEXT').subscribe((subtitle: string) => {
            let alert = this.alertCtrl.create({
              title: title,
              message: subtitle,
              buttons: ['Ok']
            });
            alert.present();
          });
          });
          return;

      }else{
        this.logservice.accion.push(" -- CODIGO SI VALIDO -- ");
        this.data.codigo =  data.idusuario;
        this.logservice.accion.push(" -- data.codigo  -- "+this.data.codigo+" ");
        
        this.actionOk();
      }
        
      
    })


  }

  actionOk(){
    //TODO: DEVOLVER LOS DATOS AQUI
   console.log("action ok");
   
   this.logservice.accion.push(" -- actionOk VALIDANDO NOMBRE CLIENTE  -- "+this.data.nombre+" ");
        
    if(this.data.nombre.length == 0){

      this.translate.get('MODAL_CLIENTE_ALERT_TITLE').subscribe((title: string) => {
      this.translate.get('MODAL_CLIENTE_ALERT_TEXT').subscribe((subtitle: string) => {
        let alert = this.alertCtrl.create({
          title: title,
          message: subtitle,
          buttons: ['Ok']
        });
        alert.present();
      });
      });
      return;
    }

    this.viewCtrl.dismiss(this.data);
  }


  actionCancelar(){
    this.logservice.accion.push(" -- actionCancelar CLIENTE  --  ");
   
     this.viewCtrl.dismiss(null);
  }
}
