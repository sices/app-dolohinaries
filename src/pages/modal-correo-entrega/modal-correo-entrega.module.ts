import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalCorreoEntregaPage } from './modal-correo-entrega';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule } from '@angular/material/';
import { MatKeyboardModule } from '@ngx-material-keyboard/core';

@NgModule({
  declarations: [
    ModalCorreoEntregaPage,
  ],
  imports: [

    // Material modules
    MatButtonModule,
    MatKeyboardModule,


    IonicPageModule.forChild(ModalCorreoEntregaPage),
    TranslateModule.forChild()
  ],
})
export class ModalCorreoEntregaPageModule {}
