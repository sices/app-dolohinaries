import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalMetodoEntregaPage } from './modal-metodo-entrega';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ModalMetodoEntregaPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalMetodoEntregaPage),
    TranslateModule.forChild()
  ],
})
export class ModalMetodoEntregaPageModule {}
