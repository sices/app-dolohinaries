import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalDuplicarPage } from './modal-duplicar';

@NgModule({
  declarations: [
    ModalDuplicarPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalDuplicarPage),
  ],
})
export class ModalDuplicarPageModule {}
