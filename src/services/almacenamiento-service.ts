import { Injectable } from '@angular/core';
import { Http, Headers, RequestMethod, URLSearchParams , RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { FotobruApiService } from './fotobru-api-service';
import { File } from '@ionic-native/file';
import { resolveDefinition } from '@angular/core/src/view/util';
declare var Fotobru : any;

@Injectable()
export class AlmacenamientoService{

    public mockData = true
    /**
     *
     */
    constructor(public http: Http, private api: FotobruApiService,private storage: Storage

        ,private file: File
    ) {

    }

    /**
     * Devuelve una promise que devuelve un arreglo de tipo, son las imagenes
     * miniatura que se muestran en el home
     *   [ 'base64img' ,'base64img' ,'base64img' ,'base64img' ]
     * @param idzona
     */
    public getFotosZonaHome(idzona){
        return this.storage.get('fotos_zonas_home_' + idzona)
    }
    public setFotosZonaHome(idzona, array){
        return this.storage.set('fotos_zonas_home_' + idzona, array)
    }
 /** ************************************************************************ ** /
    /**
     * Devuelve una promise que devuelve un arreglo de tipo
     *   [ idfoto , idfoto , idfoto, idfoto ...]
     * @param idzona
     */
    public getIdsFotosZona(idzona){
        return this.storage.get('fotosid_zonas_' + idzona);
    }
    public setIdsFotosZonas(idzona, array ){
        return this.storage.set('fotosid_zonas_' + idzona, array);
    }
 /** ************************************************************************ ** /

    /**
     * Obtiene un array de fotos (la galeria)
     */
    /*
    public getFotosGaleria(zona) {
        let _ctx = this
        return new Promise((resolve,reject) => {

          _ctx.api.obtenerApiKey((apikey) => {
            _ctx.api.buildPeticion(
              'sales/fotoszona',
              "ApiKey=" + apikey+'&idZona=5',
            ).subscribe( (data: any) => {
                console.log("Obteniendo las imagenes que en local deberia haber guardado y confirmado")
                console.log(data)
                resolve(data)
            })
          })



        })
    }
    */

    public leerFoto(){

        let directorioRaiz = this.file.dataDirectory
        //"cdvfile://localhost/persistent/"
        return this.file.readAsDataURL( directorioRaiz, 'archivoHarcodeado.jpg')
    }

    /**
     * Guarda la foto y su id en el array correspondiente de zonas
     * La foto tiene la siguiente forma.
     * {
     *      miniatura: prefix + response.Miniatura,
     *      slider: prefix + response.FotoSlider,
     *      idfoto: response.idfoto,
     *      idzona: response.idzona,
     *      nombre: response.nombre
     *  }
     * @param foto
     */
    public guardarFoto(foto, zona){
        let idzona = foto.idzona
        let idfoto = foto.idfoto
        let _ctx = this
        var promise = this.getIdsFotosZona(idzona)
        let directorioRaiz = _ctx.file.dataDirectory
        // "cdvfile://localhost/persistent/"

        var _guardar = (foto) => {

            return _ctx.file.writeFile(directorioRaiz,'archivoHarcodeado.jpg', foto.slider).then(value => {
                console.log('iPad: se guardo el archivo')
                return new Promise( (resolve,reject) => {
                    resolve('iPad: se guardo el archivo')
                })
            })
        }

        return new Promise( (resolve, reject) => {

            if( (typeof(Fotobru)) !== 'undefined' ){
                Fotobru.guardarArchivo(idzona, foto.nombre, foto.miniatura, foto.slider);
                resolve(true)
            }
            else{
                console.warn("Fotobru no esta definido")
                if(_ctx.file != null) {
                    _ctx.file.createDir(directorioRaiz, 'helloworld', false).then( value => {
                        console.log('iPad : Se creo el directorio')
                        console.log(value)
                        _guardar(foto).then( value => {
                            resolve(true)
                        }).catch( error => {
                            console.log('iPad : NO se guardo el archivo')
                            console.log(error)
                            reject(error)
                        })

                        // resolve(true)
                    }).catch( error => {
                        console.error("iPad: No se pudo crear el directorio")
                        console.error(error)

                        _guardar(foto).then( v => {
                            resolve(v)
                        }).catch(e => {
                            console.log('iPad : NO se guardo el archivo')
                            console.log(e)
                            reject(e)
                        })

                        // reject(error)
                    })
                }else
                    reject("No existe forma de almacenar la foto")
            }
        })
    }

    /**
     * Devuelve una promise que devuelve un objeto de tipo, que es la representacion real de la imagen en la app
     *  {
     *      miniatura: prefix + response.Miniatura,
     *      slider: prefix + response.FotoSlider,
     *      idfoto: response.idfoto,
     *      idzona: response.idzona,
     *      nombre: response.nombre
     *  }
     * @param idfoto
     */
    // public getFoto(idfoto){
    //     return this.storage.get('foto-'+idfoto);
    // }


}
