import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { DatePicker } from '@ionic-native/date-picker'


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { GaleriaPageModule } from '../pages/galeria/galeria.module';
import { PresentacionPageModule } from '../pages/presentacion/presentacion.module';
import { ComponentsModule } from '../components/components.module';
import { PruebaPage } from '../pages/prueba/prueba';
import { ModalDatosTargetaPageModule } from '../pages/modal-datos-targeta/modal-datos-targeta.module';
import { ModalCorreoEntregaPageModule } from '../pages/modal-correo-entrega/modal-correo-entrega.module';
import { GruposService } from '../services/grupos-service';
import { HttpModule } from '@angular/http';
import { FotobruApiService } from '../services/fotobru-api-service';
import { GaleriaService } from '../services/galeria-service';
import { LogService } from '../services/logs-service';
import { IonicStorageModule } from '@ionic/storage';
import { CarritoService } from '../services/carrito-service';
import { AlmacenamientoService } from '../services/almacenamiento-service';
import { ConfiguracionesPageModule } from '../pages/configuraciones/configuraciones.module';
import { File } from '@ionic-native/file';
import { ModalFotoFullPageModule } from '../pages/modal-foto-full/modal-foto-full.module';
import { ModalClientePageModule } from '../pages/modal-cliente/modal-cliente.module';
import { ModalMetodoEntregaPageModule } from '../pages/modal-metodo-entrega/modal-metodo-entrega.module';
import { ModalAutorizarModule } from '../pages/modal-autorizar/modal-autorizar.module';
import { ModalDuplicarPageModule } from '../pages/modal-duplicar/modal-duplicar.module';

import { DragScrollModule } from 'ngx-drag-scroll';

import {TranslateService, TranslatePipe} from '@ngx-translate/core';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';


import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/';
import { MatKeyboardModule } from '@ngx-material-keyboard/core';

import { LoggingService } from "ionic-logging-service";

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PruebaPage

  ],
  imports: [

    // Angular modules
    //  BrowserModule,
    BrowserAnimationsModule,
    FormsModule,

    // Material modules
    MatButtonModule,
    MatKeyboardModule,

    HttpModule,
    BrowserModule,
    GaleriaPageModule,
    PresentacionPageModule,
    ModalDuplicarPageModule,
    ModalDatosTargetaPageModule,
    ModalCorreoEntregaPageModule,
    ModalMetodoEntregaPageModule,
    ModalAutorizarModule,
    ModalFotoFullPageModule,
    ModalClientePageModule,
    ComponentsModule,
    HttpClientModule,
    ConfiguracionesPageModule,
    IonicModule.forRoot(MyApp,{
      mode: "ios"
    }),
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: (HttpLoaderFactory),
				deps: [HttpClient]
			}
    }),
    DragScrollModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PruebaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    File,
    GruposService,
    LoggingService,
    FotobruApiService,
    GaleriaService,
    CarritoService,
    AlmacenamientoService,
    LogService,
    DatePicker,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
