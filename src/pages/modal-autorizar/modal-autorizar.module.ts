import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalAutorizar } from './modal-autorizar';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ModalAutorizar,
  ],
  imports: [
    IonicPageModule.forChild(ModalAutorizar),
    TranslateModule.forChild()
  ],
})
export class ModalAutorizarModule {}
